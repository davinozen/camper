"""
Utility module for common RVLink url operations such as decoding, encoding, embedding/parsing flags.
"""
import urllib.parse


URL_DELIMITER = '://'
RVLINK_PROTOCOL = 'rvlink'
RVLINK_HEADER = RVLINK_PROTOCOL + URL_DELIMITER
RVLINK_BAKED_HEADER = RVLINK_PROTOCOL + 'baked/'
RVLINK_ENCODE_MODES = ['hex', 'url']
RV_FLAGS_ARG = '-flags'
RVSYNC_URL_TOKENS = ['-network', '-networkConnect']


def conform_rvlink(url):
    rvlink = str(url).strip()

    if not is_rvlink(rvlink):
        raise ValueError(f'Invalid RVLink: {url}')

    return rvlink


def is_rvlink(url: str) -> bool:
    """
    Check if specified url is using rvlink protocol

    Args:
        url (str): [description]

    Returns:
        bool: True if url is rvlink, False otherwise
    """
    return str(url).split(URL_DELIMITER)[0] == RVLINK_PROTOCOL


def is_hex_encoded_rvlink(url):
    rvlink = conform_rvlink(url)
    return rvlink.startswith(RVLINK_BAKED_HEADER) and ' ' not in rvlink


def is_url_encoded_rvlink(url):
    rvlink = conform_rvlink(url)
    return ' ' not in rvlink and '%' in rvlink


def is_encoded_rvlink(url) -> bool:
    return is_hex_encoded_rvlink(url) or is_url_encoded_rvlink(url)


def is_rvsync_url(url) -> bool:
    if not is_rvlink(url):
        return False

    for token in RVSYNC_URL_TOKENS:
        if token not in url:
            return False

    return True


def decode_rvlink(url) -> str:
    rvlink = conform_rvlink(url)

    if is_encoded_rvlink(rvlink):
        if is_hex_encoded_rvlink(rvlink):
            encoded = rvlink.replace(RVLINK_BAKED_HEADER, '')
            decoded = bytes.fromhex(encoded).decode('ascii').strip()
            return f'{RVLINK_HEADER} {decoded}'
        elif is_url_encoded_rvlink(rvlink):
            encoded = rvlink.replace(RVLINK_HEADER, '')
            decoded = urllib.parse.unquote(encoded).strip()
            return f'{RVLINK_HEADER} {decoded}'
        else:
            raise ValueError(f'Invalid RVLink encoding: {url}')

    return rvlink


def encode_rvlink(rvlink, mode='hex') -> str:
    rvlink = conform_rvlink(rvlink)

    if is_encoded_rvlink(rvlink):
        rvlink = decode_rvlink(rvlink)

    mode = mode if mode in RVLINK_ENCODE_MODES else RVLINK_ENCODE_MODES[0]

    if mode == 'url':
        rvlink = ' ' + rvlink.replace(RVLINK_HEADER, '').strip()
        encoded = urllib.parse.quote(rvlink, safe='')
        return f'{RVLINK_HEADER}{encoded}'
    elif mode == 'hex':
        rvlink = ' ' + rvlink.replace(RVLINK_HEADER, '').strip()
        encoded = rvlink.encode('hex')
        return f'{RVLINK_BAKED_HEADER}{encoded}'
    return rvlink


def parse_rvlink_args(rvlink) -> list:
    rvlink = conform_rvlink(rvlink)

    if is_encoded_rvlink(rvlink):
        rvlink = decode_rvlink(rvlink)

    rvlink_args = rvlink.replace(RVLINK_HEADER, '').strip().split('')
    rvlink_args = [arg for arg in rvlink_args if arg not in (None, '')]

    return rvlink_args


def parse_rvlink_flags(rvlink) -> list:
    rvlink_args = parse_rvlink_args(rvlink)
    flags = list()
    store_flag = False

    for arg in rvlink_args:
        if arg == RV_FLAGS_ARG:
            store_flag = True
        elif arg.startswith('-'):
            store_flag = False
        elif store_flag:
            flags.append(arg)

    return flags


def parse_rvlink_flags_dict(rvlink) -> dict:
    rvlink_args = parse_rvlink_flags(rvlink)
    flags_dict = dict()

    for arg in rvlink_args:
        if '=' in arg:
            tokens = arg.split('=', 1)
            flags_dict[str(tokens[0])] = str(tokens[1])
        else:
            flags_dict[str(arg)] = True

    return flags_dict
