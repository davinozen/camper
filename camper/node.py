"""

"""
try:
    from rv import commands, extra_commands
except ImportError:
    from .dummyrv import commands, extra_commands


from .properties import *
from .common import *
from .mu import *


CACHED_NODES = dict()
DYNAMIC_NODE_CLASSES = dict()


class Node(object):

    def __init__(self, name: str):
        self._name = str(name)
        self._type = self.__class__.__name__
        self._property_groups = dict()

    def __repr__(self) -> str:
        return '%s("%s")' % (self._type, self._name)

    def __getattr__(self, item: str) -> PropertyGroup:
        """

        Args:
            item:

        Returns:
            PropertyGroup:
        """
        if no_cache_mode():
            return PropertyGroup(self, item)

        if item not in self._property_groups:
            self._property_groups[item] = PropertyGroup(self, item)
        return self._property_groups[item]

    def __getitem__(self, item) -> PropertyGroup:
        return self.__getattr__(str(item))

    def __hash__(self):
        return hash(self.node_name())

    def property_groups(self) -> List[PropertyGroup]:
        prop_group_names = get_property_group_names(self.node_name())
        return [self.__getattr__(p) for p in prop_group_names]

    def node_properties(self) -> List[Property]:
        props = []
        for prop_group in self.property_groups():
            props.extend(prop_group.group_properties())
        return props

    def node_name(self):
        return self._name

    def fullname(self):
        return self._name

    def node_type(self):
        return self._type

    def properties_dict(self) -> PropertyValueDict:
        _dict = dict()
        for prop in self.node_properties():
            prop_name = '.'.join(prop.fullname().split('.')[1:])
            _dict[prop_name] = prop.get_values()
        return _dict

    def get_values(self, property_name: str) -> PropertyValueArray:
        tokens = property_name.split('.', 1)
        group_name = tokens[0]
        property_name = tokens[-1]
        return self.__getattr__(group_name).get_values(property_name)

    def get_value(self, property_name: str, index: int = 0) -> PropertyValue:
        return self.get_values(property_name)[index]

    def set_values(self, property_name: str, *values: PropertyValueArray):
        tokens = property_name.split('.', 1)
        group_name = tokens[0]
        property_name = tokens[-1]
        return self.__getattr__(group_name).set_values(property_name, *values)

    def set_value(self, property_name: str, value: PropertyValue):
        return self.set_values(property_name, value)

    def insert_value(self, property_name: str, index: int, value: PropertyValue):
        pass

    def insert_values(self, property_name: str, index: int, *values: PropertyValueArray):
        pass

    def input_nodes(self, traverse_groups=False):
        return input_nodes(self.node_name(), traverse_groups=traverse_groups)

    def output_nodes(self, traverse_groups=False):
        return output_nodes(self.node_name(), traverse_groups=traverse_groups)

    def set_inputs(self, input_nodes):
        input_nodes = [node.node_name() for node in input_nodes]
        commands.setNodeInputs(self.node_name(), input_nodes)

    def top_level_group(self) -> Type['Node']:
        top = extra_commands.topLevelGroup(self.node_name())
        return to_node(top) if top else self

    def has_property(self, property_name: str) -> bool:
        """
        Check if specified property exists in node.

        Args:
            property_name: '<property_group_name>.<property_name>'

        Returns:
            True if property exists, False otherwise
        """
        return commands.propertyExists(f'{self.node_name()}.{property_name}')

    def new_property(self, property_name, property_type, property_size, *values):
        tokens = property_name.split('.', 1)
        group_name = tokens[0]
        property_name = tokens[-1]
        self.__getattr__(group_name).add_property(property_name, property_type, property_size)
        if values:
            self.__getattr__(group_name).__getattr__(property_name).set_values(*values)


class LUT(Node):
    pass


class Source(Node):

    def media_paths(self):
        return self.media.movie.get_values()

    def media_path(self, index=0):
        return self.media.movie.get_value(index=index)


class RVSession(Node):
    pass


class RVFileSource(Source):
    pass


class RVImageSource(Source):
    pass


class RVCacheLUT(LUT):
    pass


class RVFormat(Node):
    pass


class RVChannelMap(Node):
    pass


class RVCache(Node):
    pass


class RVLinearize(Node):
    pass


class RVLensWarp(Node):
    pass


class RVPaint(Node):
    pass


class RVOverlay(Node):
    pass


class RVColor(Node):
    pass


class RVLookLUT(LUT):
    pass


class RVSourceStereo(Node):
    pass


class RVTransform2D(Node):
    pass


class RVRetime(Node):
    pass


class RVSequence(Node):
    pass


class RVStack(Node):
    pass


class RVSoundTrack(Node):
    pass


class RVDispTransform2D(Node):
    pass


class RVDisplayStereo(Node):
    pass


class RVDisplayColor(Node):
    pass


class RVCDL(Node):
    pass


class RVPrimaryConvert(Node):
    pass


class RVSwitch(Node):
    pass


# OCIO nodes
class OCIONode(Node):
    pass


class OCIOFile(OCIONode):
    pass


class OCIOLook(OCIONode):
    pass


class OCIODisplay(OCIONode):
    pass


# Group nodes
class Group(Node):

    def member_node(self, node_type, index=0, use_cached=True):
        members = self.member_nodes(node_type, use_cached=use_cached)
        return members[index] if members else None

    def member_nodes(self, node_type=None, use_cached=True):
        member_names = commands.nodeInGroup(self.node_name())
        member_nodes_ = [to_node(n, use_cached=use_cached) for n in member_names]

        if node_type is not None:
            if inspect.isclass(node_type):
                member_nodes_ = [n for n in member_nodes_ if isinstance(n, node_type)]
            elif isinstance(node_type, str):
                member_nodes_ = [n for n in member_nodes_ if n.node_type() == node_type]

        return member_nodes_


class RVSourceGroup(Group):

    def source_node(self):
        return self.member_node(Source, use_cached=False)

    def media_paths(self):
        return self.source_node().media_paths()

    def media_path(self, index=0):
        return self.source_node().media_path(index=index)

    def linearize_pipeline_group(self):
        return self.member_node(RVLinearizePipelineGroup)

    def color_pipeline_group(self):
        return self.member_node(RVColorPipelineGroup)

    def look_pipeline_group(self):
        return self.member_node(RVLookPipelineGroup)


class RVSequenceGroup(Group):
    pass


class RVStackGroup(Group):
    pass


class RVLayoutGroup(Group):
    pass


class RVViewGroup(Group):

    def view_pipeline_group(self):
        pass


class RVDisplayGroup(Group):

    def display_pipeline_group(self):
        return self.member_node(RVDisplayPipelineGroup)


class RVOutputGroup(Group):
    pass


class RVRetimeGroup(Group):
    pass


class RVFolderGroup(Group):
    pass


class RVSwitchGroup(Group):
    pass


# Pipeline Groups
class PipelineGroup(Group):

    def member_nodes(self, node_type=None, use_cached=True):
        pipeline_node_classes = self.pipeline.nodes.get_values()
        member_names = commands.nodeInGroup(self.node_name())
        member_names = sorted(member_names, key=lambda x: x.split('_')[2])
        member_names = member_names[:len(pipeline_node_classes)]
        member_names = sorted(member_names, key=lambda x: int(x.split('_')[2]))
        member_nodes_ = [to_node(n, use_cached=use_cached) for n in member_names]

        if node_type is not None:
            if inspect.isclass(node_type):
                member_nodes_ = [n for n in member_nodes_ if isinstance(n, node_type)]
            elif isinstance(node_type, str):
                member_nodes_ = [n for n in member_nodes_ if n.node_type() == node_type]

        return member_nodes_

    def pipeline_nodes(self, node_type=None):
        return self.member_nodes(node_type=node_type)

    def pipeline_nodes_property_dicts(self):
        nodes_properties = list()
        for n in self.pipeline_nodes():
            nodes_properties.append(
                {
                    'node_name': n.node_name(),
                    'node_type': n.node_type(),
                    'properties_dict': n.properties_dict(),
                }
            )
            return nodes_properties

    def append_pipeline_node(self, node_type: Node, properties_dict=None):
        return self.insert_pipeline_node(node_type, index=-1, properties_dict=properties_dict)

    def append_pipeline_nodes(self, node_types: Node, properties_dicts=None):
        return self.insert_pipeline_nodes(node_types, index=-1, properties_dicts=properties_dicts)

    def insert_pipeline_nodes(self, node_types, index: int = 0, properties_dicts=None):
        node_types = [n.__name__ if inspect.isclass(n) else n for n in node_types]

        if index < 0:
            index = len(self.pipeline_nodes()) + index + 1

        self.pipeline.nodes.insert_values(index, *node_types)

        if properties_dicts:
            for idx, properties_dict in enumerate(properties_dicts, index):
                if properties_dict:
                    node = self.pipeline_nodes()[idx]
                    for prop_name, prop_value in properties_dict.items():
                        if not node.has_property(prop_name):
                            node.new_property(prop_name, infer_property_type_from_value(prop_value), 1)
                        if isinstance(prop_value, (list, tuple)):
                            node.set_values(prop_name, *prop_value)
                        else:
                            node.set_values(prop_name, prop_value)

    def insert_pipeline_node(self, node_type, index=0, properties_dict=None):
        return self.insert_pipeline_nodes([node_type], index, [properties_dict] if properties_dict else None)

    def remove_pipeline_node(self, index: int = 0):
        pass

    def clear_pipeline_nodes(self):
        self.pipeline.nodes.set_values()


class RVLinearizePipelineGroup(PipelineGroup):
    pass


class RVColorPipelineGroup(PipelineGroup):
    pass


class RVLookPipelineGroup(PipelineGroup):
    pass


class RVViewPipelineGroup(PipelineGroup):
    pass


class RVDisplayPipelineGroup(PipelineGroup):
    pass


def to_node(node_name: Union[str, Type[Node]], use_cached: bool = True) -> Type[Node]:
    """

    Args:
        node_name:
        use_cached:

    Returns:

    """
    global CACHED_NODES
    node_name = str(node_name)

    if not commands.nodeExists(node_name):
        raise NameError('Node not found: {node}'.format(node=node_name))

    if use_cached and node_name in CACHED_NODES:
        return CACHED_NODES[node_name]

    node_type = str(commands.nodeType(node_name))
    try:
        node = eval(node_type)(node_name)
    except NameError:
        node = alias_type(node_type)(node_name)

    CACHED_NODES[node_name] = node
    return node


def alias_type(node_type):
    global DYNAMIC_NODE_CLASSES
    if node_type in DYNAMIC_NODE_CLASSES:
        _node_type = DYNAMIC_NODE_CLASSES[node_type]
    else:
        _node_type = type(node_type, (Node,), {})
        DYNAMIC_NODE_CLASSES[node_type] = _node_type
    return _node_type


def create_node(node_type, node_name=None):
    try:
        if not isinstance(node_type, type):
            node_type = eval(node_type)
    except NameError:
        raise NameError('Invalid node type:')

    node_name = commands.newNode(node_type.__name__, node_name)

    return to_node(node_name)


def _input_node_names(node_name, traverse_groups=False):
    return commands.nodeConnections(node_name, traverse_groups)[0]


def _output_node_names(node_name, traverse_groups=False):
    return commands.nodeConnections(node_name, traverse_groups)[1]


def input_nodes(node_name, traverse_groups=False):
    return [to_node(n) for n in _input_node_names(node_name, traverse_groups=traverse_groups)]


def output_nodes(node_name, traverse_groups=False):
    return [to_node(n) for n in _output_node_names(node_name, traverse_groups=traverse_groups)]


def connected_nodes(root_node, node_type=None, traverse_groups=False):
    ledger = list()

    def _connected_nodes(_root_node, _node_type, _traverse_groups, _ledger):
        if _node_type:
            if isinstance(_root_node, _node_type) and _root_node not in _ledger:
                _ledger.append(_root_node)
        else:
            if _root_node not in _ledger:
                _ledger.append(_root_node)

        for _node in _root_node.input_nodes(traverse_groups=traverse_groups):
            _connected_nodes(_node, _node_type, _traverse_groups, _ledger)

    for node in root_node.input_nodes(traverse_groups=traverse_groups):
        _connected_nodes(node, node_type, traverse_groups, ledger)

    return ledger


def view_node():
    return to_node(commands.viewNode())


def _source_node_names_at_frame(frame: int = None) -> List[str]:
    frame = frame if frame is not None else commands.frame()
    return [str(x) for x in commands.sourcesAtFrame(frame)]


def _source_group_names_at_frame(frame: int = None) -> List[str]:
    frame = frame or commands.frame()
    return [str(x).split('_')[0] for x in _source_node_names_at_frame(frame)]


def source_groups_at_frame(frame: int = None) -> List[RVSourceGroup]:
    frame = frame if frame is not None else commands.frame()
    return [to_node(node_name) for node_name in _source_group_names_at_frame(frame)]


def source_nodes_at_frame(frame: int = None) -> List[Union[RVFileSource, RVImageSource]]:
    frame = frame if frame is not None else commands.frame()
    return [to_node(node_name) for node_name in _source_node_names_at_frame(frame)]


def source_groups(context=SESSION_CONTEXT):
    if context == SESSION_CONTEXT:
        return nodes_of_type(RVSourceGroup)
    elif context == FRAME_CONTEXT:
        return source_groups_at_frame()
    elif context == VIEW_CONTEXT:
        view_node_ = to_node('viewGroup')
        return connected_nodes(view_node_, RVSourceGroup)
    return []


def nodes_of_type(node_type, context=SESSION_CONTEXT):
    if isinstance(node_type, Node):
        node_type = node_type.__class__.__name__
    elif issubclass(node_type, Node):
        node_type = node_type.__name__

    node_names = commands.nodesOfType(node_type)
    return [to_node(node_name) for node_name in node_names]


def per_source_member_node_by_type(group_node, node_type, source_node) -> Type[Node]:
    tokens = {
        RVTransform2D: 't',
        RVRetime: 'rt',
        RVPaint: 'p'
    }
    member_node_name = '_'.join([group_node.node_name(), tokens[node_type], source_node.node_name()])
    return to_node(member_node_name)


def source_attributes(source_group, media_name=None) -> dict:
    attributes_list = commands.sourceAttributes(source_group.node_name(), media_name)
    if attributes_list:
        return {k: v for k, v, in attributes_list}
    return dict()


def node_image_geometry(node, frame=None) -> NodeImageGeometry:
    return NodeImageGeometry(commands.nodeImageGeometry(node.node_name(), frame or commands.frame()))


def source_media_info(source_group) -> SourceMediaInfo:
    return SourceMediaInfo(commands.sourceMediaInfo(source_group.node_name()))


def get_connected_nodes(root, node_type=None, traverse_groups=False) -> List[Node]:
    ledger = list()

    def _get_connected_nodes(_root, _node_type, _traverse_groups, _ledger):
        if _node_type:
            if isinstance(_root, _node_type) and _root not in _ledger:
                _ledger.append(_root)
        else:
            if _root not in _ledger:
                _ledger.append(_root)

        for _node in _root.input_nodes(traverse_groups=traverse_groups):
            _get_connected_nodes(_node, _node_type, _traverse_groups, _ledger)

    for node in root.input_nodes(traverse_groups=traverse_groups):
        _get_connected_nodes(node, node_type, traverse_groups, ledger)

        return ledger


def source_filepath(node_name=None):
    try:
        source_group = to_node(node_name) if node_name else source_groups(FRAME_CONTEXT)[0]
        media_info = source_media_info(source_group)
        return media_info.file
    except Exception:
        Console.error(f'Failed to get filepath: {node_name}')
        return ''


def get_native_pixel(pointer_pos: Tuple[float, float], node_name: str = None) -> Tuple[float, float]:
    """

    Args:
        pointer_pos:
        node_name:

    Returns:
        xy position
    """
    source_group = to_node(node_name) if node_name else source_groups(FRAME_CONTEXT)[0]
    bot_l, bot_r, top_r, top_l = commands.imageGeometry(source_group.node_name())
    media_info = source_media_info(source_group)
    pixel_x = (pointer_pos[0] - bot_l[0]) / (bot_r[0] - bot_l[0]) * media_info.width
    pixel_y = (pointer_pos[1] - bot_r[1]) / (top_r[1] - bot_r[1]) * media_info.height
    return pixel_x, pixel_y
