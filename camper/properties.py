try:
    from rv import commands
except ImportError:
    from .dummyrv import commands


from .common import *
from .mu import *


PROPERTY_TYPE_MAPPING = {
    'string':   {'mu_type_name': commands.StringType,
                 'getter': commands.getStringProperty,
                 'setter': commands.setStringProperty,
                 'insertter': commands.insertStringProperty,
                 'py_type': str,
                 },
    'int':      {'mu_type_name': commands.IntType,
                 'getter': commands.getIntProperty,
                 'setter': commands.setIntProperty,
                 'insertter': commands.insertIntProperty,
                 'py_type': int,
                 },
    'float':    {'mu_type_name': commands.FloatType,
                 'getter': commands.getFloatProperty,
                 'setter': commands.setFloatProperty,
                 'insertter': commands.insertFloatProperty,
                 'py_type': float,
                 },
    'half':     {'mu_type_name': commands.FloatType,
                 'getter': commands.getFloatProperty,
                 'setter': commands.setFloatProperty,
                 'insertter': commands.insertFloatProperty,
                 'py_type': float,
                 },
    'byte':     {'mu_type_name': commands.FloatType,
                 'getter': commands.getFloatProperty,
                 'setter': commands.setFloatProperty,
                 'insertter': commands.insertFloatProperty,
                 'py_type': int,
                 },
}


def infer_property_type_from_value(value):
    if isinstance(value, (list, tuple)):
        _types = set()
        for v in value:
            _types.add(type(v))
        if len(_types) > 1:
            raise ValueError('Not all values are the same type.')
        return _types.pop()
    else:
        return type(value)


def get_property_names(node_name):
    return commands.properties(node_name)


def get_property_group_names(node_name):
    prop_names = get_property_names(node_name)
    return [prop_name.split('.', 2)[1] for prop_name in prop_names]


def get_property_info(property_name):
    try:
        return PropertyInfo(commands.propertyInfo(property_name))
    except Exception:
        raise PropertyNotFoundError(f'"{property_name}" is not a valid property')


def _get_py_name(mu_type_name):
    for key, value in PROPERTY_TYPE_MAPPING.items():
        if value['mu_type_name'] == mu_type_name:
            return key
    raise TypeError(f'Invalid Mu type name: {mu_type_name}')


def _value_getter(value_type):
    """

    Args:
        value_type:

    Returns:
        Callable:
    """
    return PROPERTY_TYPE_MAPPING[value_type]['getter']


def _value_setter(value_type):
    """

    Args:
        value_type:

    Returns:
        Callable:
    """
    return PROPERTY_TYPE_MAPPING[value_type]['setter']


def _value_inserter(value_type):
    """

    Args:
        value_type:

    Returns:
        Callable:
    """
    return PROPERTY_TYPE_MAPPING[value_type]['insertter']


class Property(object):
    def __init__(self, property_group, name, type_=None, size=None):
        self._property_group = property_group
        self._name = name

        if type_ and size:
            self._type = type_  # type: PropertyType
            self._size = size
        else:
            self.load_property()

        self._values = tuple(self.property_type() for _ in range(self._size))        # type: PropertyValueArray

    def load_property(self):
        info = get_property_info(self.fullname())
        self._size = info.size or 1
        self._type = _get_py_name(info.type)

    def property_name(self) -> str:
        return self._name

    def fullname(self) -> PropertyFullName:
        return f'{self._property_group.fullname()}.{self._name}'

    def property_type(self) -> PropertyType:
        return self._type

    def property_size(self) -> int:
        return self._size

    def set_value(self, value):
        self.set_values(value)

    def set_values(self, *values: PropertyValueArray):
        values = list(values)
        setter = _value_setter(self.property_type())
        return setter(self.fullname(), values, True)

    def get_value(self, index=0) -> PropertyValue:
        """

        Args:
            index:

        Returns:
            PropertyValue:
        """
        try:
            return self.get_values()[index]
        except IndexError:
            return None

    def get_values(self) -> PropertyValueArray:
        """

        Returns:
            PropertyValueArray:
        """
        if not full_cache_mode():
            getter_func = _value_getter(self._type)
            return getter_func(self.fullname())
        else:
            return self._values

    def insert_values(self, index, *values):
        values = list(values)
        insertter = _value_inserter(self.property_type())
        insertter(self.fullname(), values, index)

    def append_value(self, value):
        pass

    def append_values(self, *values):
        pass


class PropertyGroup(object):
    """
    PropertyGroups represent collections of Property objects that fall under similar categories.
    """
    def __init__(self, parent_node, group_name):
        self._parent_node = parent_node
        self._group_name = group_name
        self._properties_dict = dict()

        if no_cache_mode():
            return
        else:
            self._load_properties()

    def __getattr__(self, property_name):
        """

        Args:
            property_name:

        Returns:
            Property:
        """
        # allow use of python keywords to be used as property names if '_' is added to the end
        # ie. file_.movie.in_, node.class_, etc.
        property_name = property_name.rstrip('_')

        if no_cache_mode():
            if not self.has_property(property_name):
                raise AttributeError(f'{self._parent_node.node_name()} has no property "{property_name}"')
            return Property(self, property_name)
        else:
            return self._properties_dict[property_name]

    def __getitem__(self, property_name) -> Property:
        return self.__getattr__(str(property_name))

    def __setattr__(self, name, value):
        return self.__getattr__(name).set_values(value)

    def _load_properties(self):
        pass

    def group_name(self):
        return self._group_name

    def fullname(self):
        return f'{self._parent_node.fullname()}.{self._group_name}'

    def has_property(self, property_name) -> bool:
        return commands.propertyExists(f'{self.fullname()}.{property_name}')

    def group_properties(self) -> List[Property]:
        prop_names = get_property_names(self._parent_node.fullname())
        filtered_prop_names = list()

        for p in prop_names:
            tokens = p.split('.', 2)
            if tokens[1] == self.group_name():
                filtered_prop_names.append(tokens[-1])
        return [self.__getattr__(p) for p in filtered_prop_names]

    def add_property(self, property_name: str, property_type: PropertyType, property_size: int) -> Property:
        if property_name not in self._properties_dict:
            if not self.has_property(property_name):
                if property_type is str:
                    property_type = 'string'
                    mu_type = PROPERTY_TYPE_MAPPING[property_type]['mu_type_name']
                elif property_type is int:
                    property_type = 'int'
                    mu_type = PROPERTY_TYPE_MAPPING[property_type]['mu_type_name']
                elif property_type is float:
                    property_type = 'float'
                    mu_type = PROPERTY_TYPE_MAPPING[property_type]['mu_type_name']
                else:
                    raise TypeError(f'Cannot add property of type: {property_type}')
                property_name = commands.newProperty(f'{self.fullname()}.{property_name}', mu_type, property_size)

            self._properties_dict[property_name] = Property(self,
                                                            property_name,
                                                            property_type,
                                                            property_size)
        return self._properties_dict[property_name]

    def set_values(self, property_name: str, *values):
        return self.__getattr__(property_name).set_values(*values)

    def get_values(self, property_name):
        return self.__getattr__(property_name).get_values()
