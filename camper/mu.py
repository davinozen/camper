
from abc import ABC


class MuClass(ABC):
    """
    Python facsimile of Mu builtin classes.
    """
    def __init__(self, attribute_dict):
        self._dict = attribute_dict

    def __getattr__(self, item):
        return self._dict[item]


class ChannelInfo(MuClass):
    ...


class ChapterInfo(MuClass):
    ...


class Event(MuClass):
    ...


class IOFormat(MuClass):
    ...


class IOParameter(MuClass):
    ...


class LayerInfo(MuClass):
    ...


class MenuItem(MuClass):
    ...


class MetaEvalInfo(MuClass):
    ...


class NodeImageGeometry(MuClass):
    width: int
    height: int
    pixelAspect: int
    orientation: int


class NodeRangeInfo(MuClass):
    ...


class PixelImageInfo(MuClass):
    ...


class PropertyInfo(MuClass):
    name: str
    type: str
    dimension: int
    size: int
    userDefined: int
    info: int


class RenderedImageInfo(MuClass):
    ...


class SettingsValue(MuClass):
    ...


class SourceMediaInfo(MuClass):
    width: int
    height: int
    uncropWidth: int
    uncropHeight: int
    uncropX: int
    uncropY: int
    bitsPerChannel: int
    channels: int
    isFloat: int
    planes: int
    startFrame: int
    endFrame: int
    fps: int
    audioChannels: int
    audioRate: int
    channelInfos: int
    viewInfos: int
    defaultView: int
    file: str
    hasAudio: int
    hasVideo: int
    chapterInfos: int


class VideoDeviceState(MuClass):
    ...


class ViewInfo(MuClass):
    ...
