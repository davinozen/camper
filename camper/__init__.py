"""
This is a docstring
"""


__author__ = 'Ming (David) Zeng'
__copyright__ = 'Copyright 2018-2020, Ming (David) Zeng'
__license__ = 'MIT'
__version__ = '1.1.0'


from .common import *
from .minormode import *
from .node import *
from .properties import *
