"""
This module adds a dummy object that allows developers
"""


class DummyRV(object):

    def __init__(self, name, parent=None):
        self._name = str(name)
        self._parent = parent

    def __mro_entries__(self, bases):
        return tuple([DummyRV])

    def __getattr__(self, item):
        return DummyRV(item, parent=self)

    def __call__(self, *args, **kwargs):
        return

    @property
    def name(self) -> str:
        return self._name

    @property
    def fullname(self) -> str:
        if self._parent:
            return '.'.join([self._parent.fullname, self.name])
        return self.name


commands = DummyRV('commands')
extra_commands = DummyRV('extra_commands')
MuSymbol = DummyRV('extra_commands')
rvtypes = DummyRV('rvtypes')
