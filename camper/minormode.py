import traceback
from functools import partial


try:
    from rv import commands, rvtypes
except ImportError:
    from .dummyrv import commands, rvtypes


from .common import *


def _binding_wrapper(binding_id, event_name, *args, **kwargs):
    """
    When running a binding we wrap it in this utility function to make sure if exceptions are triggered that it's logged
    and handled, and also that we call reject() on any RV event objects to allow other bindings to use the event.

    Args:
        binding_id:
        event_name:
        *args:
        **kwargs:
    """
    try:
        MinorModeManager.run_binding(binding_id, event_name, *args, **kwargs)
    except Exception:
        traceback.print_exc()

    try:
        for arg in args:
            if arg.__class__.__name__ == 'Event' and hasattr(arg, 'reject'):
                arg.reject()
                return
    except Exception:
        traceback.print_exc()


class Binding(object):
    def __init__(self, event_name, event_handler, description, mode_name, sort_key, sort_order, flags=None, **kwargs):
        self._data = {
            'event_name': event_name,
            'event_handler': event_handler,
            'description': description,
            'mode_name': mode_name,
            'sort_key': sort_key,
            'sort_order': sort_order,
            'flags': [flags] if flags is not None and not isinstance(flags, list) else flags
        }
        if kwargs:
            self._data.update(kwargs)

    def __getattribute__(self, item):
        if item == '_data':
            return super(Binding, self).__getattribute__(item)

        try:
            return self._data[item]
        except KeyError:
            return None

    def run(self, event_name, *args, **kwargs):
        if self.qualifier:
            qualifiers = self.qualifier
            if not isinstance(qualifiers, list):
                qualifiers = [qualifiers]
            for q in qualifiers:
                if not q(*args, **kwargs):
                    return

        if self.handler:
            handlers = self.handler
            if not isinstance(handlers, list):
                handlers = [handlers]
            for h in handlers:
                h(*args, **kwargs)


class MinorModeManager(object):

    REGISTERED_MODES = list()
    REGISTERED_BINDINGS = dict()
    THREAD_SAFE_QUEUE = list()

    @classmethod
    def register_mode(cls, minor_mode):
        cls.REGISTERED_MODES.append(minor_mode)

    @classmethod
    def register_binding(cls, binding):
        cls.REGISTERED_BINDINGS[id(binding)] = binding

    @classmethod
    def unregister_binding(cls, binding):
        del cls.REGISTERED_BINDINGS[id(binding)]

    @classmethod
    def run_binding(cls, binding_id, event_name, *args, **kwargs):
        start_time = time.time()

        try:
            binding = cls.REGISTERED_BINDINGS[binding_id]
        except KeyError:
            Console.error(f'Can not find "{event_name}" binding {binding_id} ')
            return

        if binding.qualifier and not binding.qualifier(*args, **kwargs):
            return

        if binding.checker and not binding.checker(*args, **kwargs):
            binding.setup(*args, **kwargs)

        if binding.handler:
            binding.handler(*args, **kwargs)

        if not {'render', 'frame-changed'}.intersection(set(binding.event_name)):
            Console.debug(event_name, 'took: %.4fs' % (time.time() - start_time))

    @classmethod
    def make_thread_safe(cls, func, *args, **kwargs):
        if commands.isPlaying():
            cls.THREAD_SAFE_QUEUE.append(partial(func, *args, **kwargs))
        else:
            func(*args, **kwargs)

    @classmethod
    def thread_safe_run(cls, event, *args, **kwargs):
        while cls.THREAD_SAFE_QUEUE:
            func = cls.THREAD_SAFE_QUEUE.pop(0)
            func()
        event.reject()


class MinorMode(rvtypes.MinorMode):

    mode_name = None
    sort_key = None
    sort_order = None

    THREADED = 'threaded'

    def __init__(self):
        super(MinorMode, self).__init__()
        MinorModeManager.register_mode(self)

        if self.mode_name is None:
            self.mode_name = self.__class__.__name__
        if self.sort_key is None:
            self. sort_key = self.mode_name
        if self.sort_order is None:
            self.sort_order = 10

        self.bindings = dict()
        self.menu_bindings = list()
        self.event_bindings = list()
        self.menu = list()
        self.setup_bindings()
        self.setup_menu()

        self.init(self.mode_name,
                  self.event_bindings,
                  None,
                  self.menu,
                  self.sort_key,
                  self.sort_order)

    def setup_menu(self):
        pass

    def setup_bindings(self):
        pass

    def rebuild_menu(self):
        self.setup_menu()
        commands.defineModeMenu(self.mode_name, self.menu, True)

    def bind_menu(self, menu_item, menu_handler=None, shortcut=None, menu_state=None, qualifier=None, checker=None,
                  setup=None, flags=None, **kwargs):
        binding = Binding(['menu-item-clicked'],
                          menu_handler,
                          menu_item,
                          self.mode_name,
                          self.sort_key,
                          self.sort_order,
                          qualifier=qualifier,
                          checker=checker,
                          setup=setup,
                          flags=flags,
                          shortcut=shortcut,
                          menu_state=menu_state,
                          **kwargs)

        self.bindings[id(binding)] = binding
        MinorModeManager.register_binding(binding)
        return menu_item, partial(self.run_binding, id(binding), f'menu-item-clicked:{menu_item.strip()}', shortcut, menu_state)

    def bind_event(self, event_name, event_handler, description, qualifier=None, checker=None, setup=None, flags=None,
                   **kwargs):

        if not isinstance(event_name, list):
            event_name = [event_name]

        binding = Binding(event_name,
                          event_handler,
                          description,
                          self.mode_name,
                          self.sort_key,
                          self.sort_order,
                          qualifier=qualifier,
                          checker=checker,
                          setup=setup,
                          flags=flags,
                          **kwargs)

        self.bindings[id(binding)] = binding
        MinorModeManager.register_binding(binding)

        for name in event_name:
            self.event_bindings.append((name, partial(self.run_binding, id(binding), name), description))

    def dynamic_bind(self, event_name, event_handler, description, qualifier=None, checker=None, setup=None, flags=None,
                     **kwargs):

        if not isinstance(event_name, list):
            event_name = [event_name]

        binding = Binding(event_name,
                          event_handler,
                          description,
                          self.mode_name,
                          self.sort_key,
                          self.sort_order,
                          qualifier=qualifier,
                          checker=checker,
                          setup=setup,
                          flags=flags
                          ** kwargs)

        self.bindings[id(binding)] = binding
        MinorModeManager.register_binding(binding)

        for name in event_name:
            commands.bind(self.mode_name, 'global', name, partial(self.run_binding, id(binding), name), description)

    def unbind_event(self, event_name):
        commands.unbind(self.mode_name, 'global', event_name)
        to_unbind = list()
        for binding_id, binding in self.bindings.items():
            if event_name in binding.event_name:
                if len(binding.event_name) == 1:
                    to_unbind.append((binding_id, binding))
                else:
                    binding.event_name.remove(event_name)

        for binding_id, binding in to_unbind:
            del self.bindings[binding_id]
            MinorModeManager.unregister_binding(binding)

    @staticmethod
    def run_binding(binding_id, event_name, *args, **kwargs):
        try:
            MinorModeManager.run_binding(binding_id, event_name, *args, **kwargs)
        except Exception:
            traceback.print_exc()

        try:
            for arg in args:
                if arg.__class__.__name__ == 'Event' and hasattr(arg, 'reject'):
                    arg.reject()
                    break
        except Exception:
            print('Error encountered calling event.reject()')
