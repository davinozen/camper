"""
This module provides common functions, constants, and variables for use in all other modules. As such this module
must NEVER import from other camper modules.
"""
import functools
import inspect
import time
from typing import *


try:
    from rv import commands, extra_commands
except ImportError:
    from .dummyrv import commands, extra_commands
try:
    from pymu import MuSymbol
except ImportError:
    from .dummyrv import MuSymbol


# Node cache modes
NO_CACHE = 0
HALF_CACHE = 1
FULL_CACHE = 2
VALID_NODE_CACHE_MODES = [NO_CACHE, HALF_CACHE, FULL_CACHE]
_NodeCacheMode = NO_CACHE


# Node query contexts
FRAME_CONTEXT = 0
VIEW_CONTEXT = 1
SESSION_CONTEXT = 2
PRINT_TIME = False


# Type hinting
PropertyFullName = NewType('PropertyFullName', str)
"""
Full name of a property which includes the node name, property group name, and property name
"""
PropertyValue = Optional[Union[int, float, str]]
"""
Any set of values that are returned by Mu
"""
PropertyValueArray = Tuple[PropertyValue]
"""
An array of Property objects
"""
PropertyValueDict = Dict[PropertyFullName, PropertyValueArray]
"""
Value dictionary
"""
PropertyType = Type[PropertyValue]
"""
Supported types are int, float, and str
"""


class MuWrapper(object):
    """
    MuWrapper is an attempt to provide a Pythonic way to call Mu code in RV without using MuSymbols. This provides the
    ability to copy and paste Mu code from Tweak's support documents and/or publicly available Mu code and use it in
    python.
    """
    def __init__(self, parent=None, name=None):
        self._parent = parent
        self._name = name
        if self._parent and self._parent._full_name:
            self._full_name = '.'.join([self._parent._full_name, self._name])
        else:
            self._full_name = self._name

    def __getattr__(self, item):
        return MuWrapper(self, item)

    def __call__(self, *args, **kwargs):
        return MuSymbol(self._full_name)(*args, **kwargs)


class Console(object):
    """
    Console output needs to be formatted correctly to display in the RV Console. This class provides convenient methods
    for outputting to the RV Console.
    """
    @staticmethod
    def _format_args(*args):
        try:
            mode_name = str(args[0].mode_name)
            formatted = tuple([mode_name + ':']) + args[1:]
            return formatted
        except AttributeError:
            pass
        return args

    @classmethod
    def debug(cls, *args):
        formatted_args = cls._format_args(args)
        print('DEBUG:' + ' '.join([str(arg for arg in formatted_args)]))

    @classmethod
    def info(cls, *args):
        formatted_args = cls._format_args(args)
        print('INFO:' + ' '.join([str(arg for arg in formatted_args)]))

    @classmethod
    def warning(cls, *args):
        formatted_args = cls._format_args(args)
        print('WARNING:' + ' '.join([str(arg for arg in formatted_args)]))

    @classmethod
    def error(cls, *args):
        formatted_args = cls._format_args(args)
        print('ERROR:' + ' '.join([str(arg for arg in formatted_args)]))


class PropertyError(Exception):
    pass


class PropertyNotFoundError(PropertyError):
    pass


def no_cache_mode() -> bool:
    """
    Check camper mode

    Returns:
        True if camper is in live mode, False otherwise
    """
    return _NodeCacheMode == NO_CACHE


def full_cache_mode() -> bool:
    return _NodeCacheMode == FULL_CACHE


def node_cache_mode() -> int:
    return _NodeCacheMode


def set_node_cache_mode(mode: int = 0):
    if mode not in VALID_NODE_CACHE_MODES:
        raise ValueError(f'Invalid mode {mode}, choose from {VALID_NODE_CACHE_MODES}')
    _NodeCacheMode = mode


def display_feedback(message, duration=3.0):
    extra_commands.displayFeedback2(message, duration)


def timeit(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        if PRINT_TIME:
            print('{time:3f}s | {module} | {func_name}'.format(time=time.time() - start,
                                                               module=str(inspect.getmodule(func).__name__),
                                                               func_name=func.__name__))
        return result
    return wrapper


def disabled_menu():
    return commands.DisabledMenuState


def checked_menu():
    return commands.CheckedMenuState


def unchecked_menu():
    return commands.UncheckedMenuState


def menu_separator():
    return '_', None
