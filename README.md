Camper
===========

[![PyPI version shields.io](https://img.shields.io/pypi/v/camper.svg)](https://pypi.python.org/pypi/camper/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/camper.svg)](https://pypi.python.org/pypi/camper/)
[![PyPI license](https://img.shields.io/pypi/l/camper.svg)](https://pypi.python.org/pypi/camper/)
[![PyPI status](https://img.shields.io/pypi/status/camper.svg)](https://pypi.python.org/pypi/camper/)


Camper provides developers with a more Pythonic API to interact with Tweak RV 


```python
SOME_PATH = '/some/path/to/media.mov'

# Native RV API
from stubs import commands


# Camper
import camper
top_source_group = camper.source_groups(camper.FRAME_CONTEXT)[0]        # get topmost RVSourceGroup in current frame
file_source = top_source_group.source_node()                    # get RVFileSource inside RVSourceGroup
file_source.media.movie.set_value(SOME_PATH)                    # set new path

```


```python
# Native RV API
from stubs import commands



# Camper
import camper
top_source_group_node = camper.source_node(camper.FRAME_CONTEXT)[0]     # get the topmost RVSourceGroup
file_source_group = top_source_group_node.source_node()         # get the RVFileSource inside the RVSourceGroup
print(file_source_group.media_movie())                          # print media paths

```