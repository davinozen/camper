"""
ascii files are used only for docstrings and module constants
texinfo files are used to parse class and function signatures

"""

import os
import re
from pprint import pprint
import traceback

supported_modules = [
    'commands',
    'extra_commands',
    # 'runtime',
    # 'rvtypes',
    # 'rvui',
]

snake_case_re = r'[a-z]+(?:_[a-z]+)*'           # modules_use_snake_case
pascal_case_re = r'(?:[A-Z]+[a-z]*)+'           # ClassesUseCamelCase
camel_case_re = r'[a-z]+(?:[A-Z][a-z]+)*'       # functionsUseLowerCamelCase


texinfo_def_re = re.compile(
    r'@(?P<def_tag>def[a-z]+) '
    r'{(?P<category>(Function|Type|Alias))?} '
    r'{(?P<data_type>.+)?} '
    fr'(?P<name>{camel_case_re}) '
    r'\((?P<return_type>.+?);(?P<arguments>.+?)\)'
    r'\n@end (?P=def_tag)\n'
)


texinfo_arg_re = re.compile(
    r'(?P<arg_type>.+)'
    fr'\s+@var{{(?P<arg_name>{camel_case_re})}}(=\s+(?P<default_value>\S+))?'
)


texinfo_ret_re = re.compile(
    r'.+'
)

MU_TYPE_ALIAS = {
    'void': 'None',
    'nil': 'None',
    'char': 'chr',
    'double': 'float',
    'object': 'object',
    'short': 'int',
    'bool': 'bool',
    'string': 'str',
    'half': 'float',
    'int64': 'int',
    'int': 'int',
    'byte': 'int',
    'float': 'float',
    '(string, string)': 'Tuple[str]'
}

TYPE_ALIAS_RE = fr'({"|".join(list(MU_TYPE_ALIAS.keys()) + [pascal_case_re])})'
LIST_OF_VARIABLES_1 = re.compile(fr'^(?P<alias>{TYPE_ALIAS_RE})\[]$')
LIST_OF_VARIABLES_2 = re.compile(fr'^\[(?P<alias>{TYPE_ALIAS_RE})]$')


def replace_data_type(type_string):
    type_string = type_string.strip()

    if type_string in MU_TYPE_ALIAS:
        return MU_TYPE_ALIAS[type_string]

    match = re.match(LIST_OF_VARIABLES_1, type_string) or re.match(LIST_OF_VARIABLES_2, type_string)
    if match:
        #print(f'List[{MU_TYPE_ALIAS.get(match.group("alias"), match.group("alias"))}]')
        return f'List[{MU_TYPE_ALIAS.get(match.group("alias"), match.group("alias"))}]'

    if ';' in type_string:
        return 'Callable'

    return MU_TYPE_ALIAS.get(type_string, type_string)

import inspect


def parse_texinfo_dict(texinfo_lines, module_name):
    texinfo_dict = dict()
    for match in re.finditer(texinfo_def_re, texinfo_lines):
        arg_strings = list()
        arg_tokens = match.group('arguments').split(',')
        for arg_token in arg_tokens:
            arg_match = re.match(texinfo_arg_re, arg_token.strip())
            if arg_match:
                arg_strings.append(f'{arg_match.group("arg_name")}: '
                                   f'{replace_data_type(arg_match.group("arg_type"))}'
                                   f'{" = %s" % arg_match.groupdict().get("default_value") if "default_value" in match.groupdict() else ""}')

        ret_strings = list()
        ret_tokens = match.group('return_type').split(',')
        for ret_token in ret_tokens:
            ret_match = re.match(texinfo_ret_re, ret_token.strip())
            if ret_match:
                ret_strings.append(replace_data_type(ret_match.group(0)))

        if match.group('name') == 'bindings':
            print(ret_strings)

        texinfo_dict[match.group('name')] = {
            'category': match.group('category'),
            'args': arg_strings,
            'definition': f'({match.group("return_type")};{match.group("arguments")})',
            'ret': ret_strings
        }
    return texinfo_dict


def parse_ascii_dict(ascii_lines, module_name):
    ascii_constant_int_re = re.compile(
        fr' {{4}}{module_name}.(?P<name>{pascal_case_re}) = int (?P<value>-?\d+)'
    )

    section_chunks = ascii_lines.split('\n    ')
    #pprint(section_chunks)

    ascii_doc_re = re.compile(
        fr'{module_name}\.(?P<name>{camel_case_re}) \(.+\)\n(?P<docstring>.+)',
        flags=re.DOTALL
    )

    ascii_dict = dict()
    for match in re.finditer(ascii_constant_int_re, ascii_lines):
        ascii_dict[match.group('name')] = {
            'category': 'Constant',
            'value': match.group('value')
        }

    for section in section_chunks:
        match = re.match(ascii_doc_re, section)
        if match:
            ascii_dict[match.group('name')] = {
                'category': 'Function',
                'docstring': match.group('docstring')
            }

    return ascii_dict


def pyi_format(doc_dict):
    pyi_text = 'from typing import *\n'
    pyi_text += 'from ._builtins import *\n'
    pyi_text += '\n\n'

    constants_str = ''
    alias_str = ''
    type_str = ''
    funcs_str = ''

    for member_name in sorted(doc_dict.keys()):

        if doc_dict[member_name]['category'] == 'Constant':
            constants_str += f'{member_name} = {doc_dict[member_name]["value"]}\n'
        elif doc_dict[member_name]['category'] == 'Type':
            pass
        elif doc_dict[member_name]['category'] == 'Alias':
            pass
        elif doc_dict[member_name]['category'] == 'Function':
            func_str = f'def {member_name}' \
                       f'({", ".join(doc_dict[member_name]["args"])})' \
                       f' -> {", ".join(doc_dict[member_name]["ret"])}:'
            docstring = doc_dict[member_name].get('docstring')
            if docstring:
                docstring = '    ' + docstring.replace('\n', '\n    ')
                func_str += '\n    """' \
                            f'\n{docstring}' \
                            '\n    """'
            func_str += '\n    ...' \
                        '\n\n\n'
            funcs_str += func_str

    if constants_str:
        pyi_text += constants_str
        pyi_text += '\n\n'

    if alias_str:
        pyi_text += alias_str
        pyi_text += '\n\n'

    if type_str:
        pyi_text += type_str
        pyi_text += '\n\n'

    if funcs_str:
        pyi_text += funcs_str

    return pyi_text


def autogen_from_docs():
    current_directory = os.path.dirname(__file__)
    raw_directory = os.path.join(current_directory, '_raw')

    for module_name in supported_modules:
        try:
            module_dict = dict()

            texinfo_path = os.path.join(raw_directory, module_name + '.texinfo')
            with open(texinfo_path, 'r') as _f:
                texinfo_lines = _f.read()
            module_dict.update(parse_texinfo_dict(texinfo_lines, module_name))

            ascii_path = os.path.join(raw_directory, module_name + '.ascii')
            with open(ascii_path, 'r') as _f:
                ascii_lines = _f.read()
            ascii_dict = parse_ascii_dict(ascii_lines, module_name)

            for k in ascii_dict:
                if k in module_dict:
                    module_dict[k].update(ascii_dict[k])

            pyi_lines = pyi_format(module_dict)

            with open(os.path.join(current_directory, module_name + '.pyi'), 'w+') as _f:
                _f.write(pyi_lines)
        except IOError as _e:
            print(f'Error Encountered: {module_name}')
            traceback.print_exc()


if __name__ == '__main__':
    autogen_from_docs()
