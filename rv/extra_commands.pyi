from typing import *
from ._builtins import *


def activatePackageModeEntry(modeModuleName: str) -> rvtypes.Mode:
    ...


def activateSync() -> None:
    ...


def appendToProp(name: str, value: str) -> None:
    ...


def associatedNode(typeName: str, nodeName: str) -> str:
    """
    Find the node of type typeName which is associated with the given node
    nearest the given node. Normally the given node will be a source node
    and the type will something like RVColor. associatedNode walks down
    the DAG starting at nodeName until it finds a node that matches the
    input type. It will search all roots.
    This replaces all the similarily named functions in packages and
    elsewhere called associatedNode which was merely fabricating the node
    name based on the input name.
    """
    ...


def associatedNodes(typeName: str, nodeName: str) -> List[str]:
    """
    Find all nodes of type typeName associated with the given
    node. associatedNodes walks down the DAG starting at nodeName (often a
    leaf) and makes an array of all nodes of type typeName.
    """
    ...


def associatedVideoDevice(displayGroup: str) -> str:
    ...


def cacheUsage() -> (float, float, int[]):
    ...


def centerResizeFit() -> None:
    ...


def cprop(name: str, propType: int) -> None:
    ...


def currentImageAspect() -> float:
    ...


def cycleNodeInputs(node: str, forward: bool) -> None:
    """
    Cycle inputs on a node
    """
    ...


def deactivatePackageModeEntry(modeModuleName: str) -> rvtypes.Mode:
    ...


def displayFeedback(text: str, duration: float, g: Callable) -> None:
    ...


def existsInProp(name: str, value: str) -> bool:
    ...


def findAnnotatedFrames() -> List[int]:
    """
    Returns an array of all annotated frames relative to the view node. The array
    is not sorted and some frames may appear more than once.
    """
    ...


def frameImage() -> None:
    ...


def inputNodeUserNameAtFrame(frame: int) -> str:
    """
    Locate the input in the eval path at frame starting at node and return its ui name.
    """
    ...


def isNarrowed() -> bool:
    ...


def isPlayable() -> bool:
    ...


def isPlayingBackwards() -> bool:
    ...


def isPlayingForwards() -> bool:
    ...


def isSessionEmpty() -> bool:
    ...


def isViewNode(name: str) -> bool:
    ...


def loadCurrentSourcesChangedFrames() -> None:
    ...


def nodesInEvalPath(frame: int, typeName: str, nodeName: str) -> List[str]:
    """
    Find the nodes of type typeName which are in the evaluation path
    between the root and the given node. 
    If typeName is nil (default) then all the nodes are returned.
    If nodeName is nil (default) then all leaf paths will be searched.
    """
    ...


def nodesInGroupOfType(groupNode: str, typeName: str) -> List[str]:
    """
    Return list of node names in the given group, with type equal to the given type.
    """
    ...


def nodesUnderPointer(event: Event, typeName: str) -> string[][]:
    ...


def numFrames() -> int:
    ...


def popInputToTop(node: str, input: str) -> None:
    """
    Reorder inputs of a node so that index is the first input. The ordering is stable.
    """
    ...


def recordPixelInfo(event: Event) -> None:
    ...


def reloadInOut() -> None:
    ...


def removeFromProp(name: str, value: str) -> None:
    ...


def scale() -> float:
    ...


def sequenceBoundaries(node: str) -> List[int]:
    ...


def set(name: str, value: List[int]) -> None:
    ...


def setActiveState() -> None:
    ...


def setDisplayProfilesFromSettings() -> List[str]:
    ...


def setInactiveState() -> None:
    ...


def setScale(s: float) -> None:
    ...


def setTranslation(t: vector float[2]) -> None:
    ...


def sourceFrame(atframe: int, root: str) -> int:
    ...


def sourceImageStructure(n: str, m: str) -> (int, int, int, int, bool, int):
    ...


def sourceMetaInfoAtFrame(atframe: int, root: str) -> MetaEvalInfo:
    ...


def stepBackward(n: int) -> None:
    ...


def stepForward(n: int) -> None:
    ...


def toggleFilter() -> None:
    ...


def toggleForwardsBackwards() -> None:
    ...


def toggleFullScreen() -> None:
    ...


def toggleMotionScope() -> None:
    ...


def toggleMotionScopeFromState(state: rvtypes.State) -> None:
    ...


def togglePlay() -> None:
    ...


def togglePlayIfNoScrub() -> None:
    ...


def togglePlayVerbose(verbose: bool) -> None:
    ...


def toggleRealtime() -> None:
    ...


def toggleSync() -> None:
    ...


def topLevelGroup(innode: str) -> str:
    ...


def translation() -> vector float[2]:
    ...


def uiName(innode: str) -> str:
    ...


def updatePixelInfo(event: Event) -> None:
    ...


