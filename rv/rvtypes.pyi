from typing import *
from ._builtins import *


def activate(this: rvtypes.Mode) -> None:
    ...


def back() -> rvtypes.Widget&:
    ...


def cancel(this: rvtypes.ExternalProcess) -> None:
    ...


def clear() -> None:
    ...


def clearQuitMessages(this: rvtypes.State) -> None:
    ...


def configPath(this: rvtypes.Mode, packageName: str) -> str:
    ...


def contains(this: rvtypes.Widget, p: vector float[2]) -> bool:
    ...


def deactivate(this: rvtypes.Mode) -> None:
    ...


def defineEventTable(this: rvtypes.MinorMode, tableName: str, bindings: string)]) -> None:
    ...


def defineEventTableRegex(this: rvtypes.MinorMode, tableName: str, bindings: string)]) -> None:
    ...


def drag(widget: rvtypes.Widget, event: Event) -> None:
    ...


def drawInMargin(this: rvtypes.Widget, whichMargin: int) -> None:
    ...


def empty() -> bool:
    ...


def erase() -> None:
    ...


def finish(this: rvtypes.ExternalProcess, exitcode: int) -> None:
    ...


def front() -> rvtypes.Widget&:
    ...


def getQuitMessage(this: rvtypes.State) -> str:
    ...


def init(this: rvtypes.Widget, name: str, blist: string)], allowMultipleInstances: bool) -> None:
    ...


def inside(this: rvtypes.Widget.Button, x: int, y: int) -> bool:
    ...


def isActive(this: rvtypes.Mode) -> bool:
    ...


def isFinished(this: rvtypes.ExternalProcess) -> bool:
    ...


def layout(this: rvtypes.Widget, event: Event) -> None:
    ...


def move(widget: rvtypes.Widget, event: Event) -> None:
    ...


def name(this: rvtypes.Mode) -> str:
    ...


def registerQuitMessage(this: rvtypes.State, registrar: str, message: str) -> None:
    ...


def release(widget: rvtypes.Widget, event: Event, closeFunc: Callable) -> None:
    ...


def render(this: rvtypes.Widget, event: Event) -> None:
    ...


def requiredMarginValue(this: rvtypes.Widget) -> float:
    ...


def resize() -> None:
    ...


def rest() -> rvtypes.Widget[]:
    ...


def setMenu(this: rvtypes.MinorMode, menu: List[MenuItem]) -> None:
    ...


def setMenuStrict(this: rvtypes.MinorMode, menu: List[MenuItem]) -> None:
    ...


def size() -> int:
    ...


def storeDownPoint(widget: rvtypes.Widget, event: Event) -> None:
    ...


def supportPath(this: rvtypes.Mode, moduleName: str) -> str:
    ...


def toggle(this: rvtypes.Widget) -> None:
    ...


def toggleScrubClamps(this: rvtypes.State) -> None:
    ...


def toggleStepWraps(this: rvtypes.State) -> None:
    ...


def unregisterQuitMessage(this: rvtypes.State, registrar: str) -> None:
    ...


def updateBounds(this: rvtypes.Widget, minp: vector float[2], maxp: vector float[2]) -> None:
    ...


def updateMargins(this: rvtypes.Widget, activated: bool) -> None:
    ...


def urlDropFunc(this: Callable, url: str) -> ((void:
    ...


