from typing import *
from ._builtins import *
from rv import qt


def activateMode(name: str) -> None:
    """
    Turn on the event table(s) for the given mode. After calling
    activateMode() events will be filtered through the mode's event table.
    """
    ...


def activeEventTables() -> List[str]:
    """
    Returns a list of the event table names current active.
    """
    ...


def activeModes() -> List[str]:
    """
    Returns a list of active mode names.
    """
    ...


def addSource(fileNames: List[str], tag: str) -> None:
    """
    Add a new source group to the session. This results in the creation of
    an RVFileSource node and additional RVSourceGroup nodes like color, etc. 
    A "incoming-source-path" event is generated when this is called.
    A "new-source" event will also be generated after "incoming-source-path".
    If an array of filenames are provided, they are added as layers in a single
    source.  For example left and right eyes, or audio and video files.
    An optional tag can be provided which is passed to the generated
    internal events. The tag can indicate the context in which the
    addSource() call is occuring (e.g. drag, drop, etc).
    """
    ...


def addSourceVerbose(fileNames: List[str], tag: str) -> str:
    """
    Similar to addSource(), but returns the name of the source node created.
    """
    ...


def addSources(fileNames: List[str], tag: str, processOpts: bool, merge: bool) -> None:
    """
    Add a new source group to the session (see addSource). This function adds
    the requested sources asyncronously.  In addition to the "incoming-source-path" and 
    "new-source" events generated for each source.  A "before-progressive-loading" and 
    "after-progressive-loading" event pair will be generated at the appropriate times.
    An optional tag can be provided which is passed to the generated
    internal events. The tag can indicate the context in which the
    addSource() call is occuring (e.g. drag, drop, etc).
    The optional processOpts argument can be set to true if there are 'option' states
    like -play, that should be processed after the loading is complete.
    Note that sources can be single movies/sequences or you can use the "[]"
    notation from the command line to specify multiple files for the source, like
    stereo layers, or additional audio files.  Per-source command-line flags can
    also be used here, but the flags should be marked by a "+" rather than a "-".
    Also note that each argument is a separate element of the input string array.  
    For example a single stereo source might look like string[] 
    {"[", "left.mov", "right.mov", "+rs", "1001", "]" }
    """
    ...


def addToSource(sourceNode: str, fileName: str, tag: str) -> None:
    """
    for information about the tag.
    """
    ...


def alertPanel(associated: bool, type: int, title: str, message: str) -> int:
    """
    Launch a dialog box to alert the user to something important.
    If associated is true on the Mac, the alert dialog will be a "sheet" on 
    other platforms it has no effect. 
    The type can be one of InfoAlert, WarningAlert, or ErrorAlert.
    The title and message are displayed on the dialog box in a platform specific way.
    One or more buttons can be defined. The button0 must be defined (non-nil). The
    other buttons (button1 and button2) can be nil to hide them.
    The function will return when one of the three buttons is pressed by the user.
    The number of the button pressed is returned.
    """
    ...


def audioCacheMode() -> int:
    """
    Returns the value set by setAudioCacheMode().
    """
    ...


def audioTextureComplete() -> float:
    ...


def beginCompoundCommand(name: str) -> None:
    ...


def bgMethod() -> str:
    """
    Return the background draw method as a string.
    """
    ...


def bind(modeName: str, tableName: str, eventName: str, func: Callable, description: str) -> None:
    """
    Bind an event function to an event. If a the named event table does
    not yet exist in the specified mode it is created. 
    An event function must have the signature: (void; Event). The Event
    object can be querried and state can be set on it in response to the
    event. If the event function does not call the reject() function on
    the event object, then the event is considered accepted and no further
    matching event functions will be called. If, on the other hand,
    reject() is called and another function is bound to the same event,
    then it will also be called. This process continues until reject() is
    not called (the event is accepted) or all bound event functions have
    been called.
    The description string should indicate succinctly (i.e. a sentence or
    less) what the event function does.
    """
    ...


def bindRegex(modeName: str, tableName: str, eventPattern: str, func: Callable, description: str) -> None:
    """
    Bind an event function to any event that matches the given posix
    regular expression. Regular expression bindings have lower precedence
    than normal bindings so they will never orderride a normal
    binding in the same table.
    See also: bind()
    """
    ...


def bindingDocumentation(eventName: str, modeName: str, tableName: str) -> str:
    """
    Returns the documentation associated with the given binding of
    eventName in mode modeName and table tableName.
    """
    ...


def bindings() -> List[Tuple[str]]:
    """
    Returns a array of tuples of all currently active bound events and the
    assocated documentation. This returns events from all currently active
    event tables in the order in which they are scanned.
    """
    ...


def cacheDir() -> str:
    """
    Location of http network object caching
    """
    ...


def cacheInfo() -> (int, int, int, float, float, float, List[int]):
    """
    Returns a tuple of information about cache usage:
    |===================================================
    | int64 | capacity of the cache in bytes
    | int64 | bytes used by cache
    | int64 | look ahead bytes used
    | float | look ahead seconds
    | float | look ahead wait time in seconds
    | float | audio cache time in seconds
    | int[] | array of start-end frames of cached ranges
    |===================================================
    """
    ...


def cacheMode() -> int:
    """
    Returns the value set by setCacheMode().
    """
    ...


def cacheOutsideRegion() -> bool:
    ...


def cacheSize() -> int:
    """
    Returns the available memory.
    """
    ...


def center() -> None:
    """
    Center the window on screen.
    """
    ...


def clearAllButFrame(frame: int) -> None:
    """
    Clear the frame cache except for the given frame.
    """
    ...


def clearHistory() -> None:
    ...


def clearSession() -> None:
    """
    Clear all sources from the current session. This will result in an empty session.
    """
    ...


def close() -> None:
    """
    Close the session window and exit the session.
    """
    ...


def closestNodesOfType(typeName: str, root: str, depth: int) -> List[str]:
    """
    This function will scan the graph in input order (determined by
    scaning inputs in order) from the given root node or the current view
    node if none is specified.
    When a node matching the given type is found, and the depth is 0, the
    branch will stop being searched for additional nodes and the next
    branch will be searched.
    For depth greater than 0, the function will continue searching
    branches until N nodes of the type are found where N is the depth + 1.
    Basically, this function is useful to find the nearest nodes to the
    view node of a given type without worrying about evaluation order. So
    in that sense it is similar to `metaEvaluateClosestByType()` . For
    example this function is useful to find the nearest `RVPaint` nodes to
    modify for annotation or to find the nearest transform nodes that
    would be modifiable for a transform manipulator.
    """
    ...


def commandLineFlag(flagName: str, defaultValue: str) -> str:
    """
    The RV command line has an option `-flags` . Arguments after `-flags` can
    have the form `foo` or `foo=bar` . `commandLineFlag()` will find the value 
    of one of these name value pairs. If the argument does not have a value, 
    then true will be returned.
    For example:
    ----
    shell> rv -flags A B=foo
    ----
    `commandLineFlag("A")` would return `true`
    `commandLineFlag("B")` would return `foo`
    """
    ...


def contentAspect() -> float:
    """
    Returns a float value for the aspect ratio of the currently viewed source. If
    the view contains multiple sources then the first rendered image is used.
    """
    ...


def contractSequences(files: List[str]) -> List[str]:
    """
    Given an array of file names, the function will attempt to contract
    them into image sequence notation. For example, the files:
    ----
    foo.0001.tif
    foo.0002.tif
    foo.0003.tif
    foo.0004.tif
    ----
    would result in a single sequence named foo.#.tif. See the rv user's
    manual for information about sequence notation.
    """
    ...


def currentFrameStatus() -> int:
    ...


def data() -> object:
    """
    Returns the object given to setData(). This is the rvtypes.State object.
    """
    ...


def deactivateMode(name: str) -> None:
    """
    Turn off the event table(s) for the given mode.
    """
    ...


def decodePassword(password: str) -> str:
    """
    -
    """
    ...


def defineMinorMode(name: str, sortKey: str, order: int) -> None:
    """
    Creates a (low level) minor mode and space for associated event
    tables. The name is assigned to the mode. A mode at this level is an
    object with the following attributes:
     * Name
     * Set of named event tables
     * Sort key and order number
     * Menu 
    When a mode is active its menu is merged into the main menu of the
    application and its event tables are used to handle events. The order
    in which the mode's event tables are scanned is determined by the sort
    key and the order number. The modes are sorted lexically using the
    sort key; if two modes have the same sort key, then the order value is
    used as the key the modes with common sort keys.
    There are two types of modes: major and minor. There can be any number
    of minor modes active at a time but only a single major mode, and the
    active major mode has higher precedence than any minor mode when
    scanning event tables (i.e. the major mode gets events first).
    Each mode can have multiple named event tables which can be activated
    independently as long as the mode itself is active. The activation
    history of event tables is a stack which can be pushed and poped. Its
    also possible to deactive them in any order if needed.
    Each event table has a bounding box. The bounding box is only used
    with pointer events or events that have a screen location. Only events
    with a location that falls inside of the bounding box are considered.
    """
    ...


def defineModeMenu(mode: str, menu: List[MenuItem], strict: bool) -> None:
    """
    Define the menu structure associated with the mode. The menu is
    automatically merged into the main menu when the mode is active. When
    the mode is inactive the menu is not part of the main menu.
    Setting the strict option to true will replace the menu instead of
    merging with an existing menu of the same name.
    """
    ...


def deleteNode(nodeName: str) -> None:
    """
    Delete the node created by newNode().
    """
    ...


def deleteProperty(propertyName: str) -> None:
    """
    Delete a property created with newProperty().
    """
    ...


def editNodeSource(nodeName: str) -> None:
    ...


def editProfiles() -> None:
    ...


def elapsedTime() -> float:
    """
    Elapsed time since user timer start in seconds.
    """
    ...


def encodePassword(password: str) -> str:
    """
    -
    """
    ...


def endCompoundCommand() -> None:
    ...


def eval(text: str) -> str:
    """
    Evalutes a string as Mu source. The resulting value is converted to a
    string and returned. Eval runs in the context of the application so
    any modules loaded into the app will be visible to the code.
    """
    ...


def eventToCameraSpace(sourceName: str, point: Tuple[float, float]) -> Tuple[float, float]:
    """
    Given a point in event space (whatever the Event object returns for
    the pointer value) and the name of a source node/path, returns the
    coordinate in camera space [0, aspect] x [0, 1].
    """
    ...


def eventToImageSpace(sourceName: str, point: Tuple[float, float], normalizedImageOrigin: bool) -> Tuple[float, float]:
    """
    Given a point in event space (whatever the Event object returns for
    the pointer value) and the name of a source node/path, returns the
    coordinate in local image coordinates.
    """
    ...


def existingFilesInSequence(sequence: str) -> List[str]:
    """
    Given a "sequence spec" like "foo.101-200#.jpg", returns a list of all files in the seuquence that actually
    exist.
    """
    ...


def existingFramesInSequence(sequence: str) -> List[int]:
    ...


def exportCurrentFrame(filename: str) -> None:
    """
    Take a snapshot of the current view window and save it as an image file.
    The file name extension is used to determine the format.
    """
    ...


def exportCurrentSourceFrame(filename: str) -> None:
    """
    Save the current source image as an image file.
    The file name extension is used to determine the format.
    """
    ...


def fileKind(filename: str) -> int:
    """
    Returns one of: ImageFileKind, MovieFileKind, LUTFileKind, DirectoryFileKind,
    RVFileKind, EDLFileKind, or UnknownFileKind to indicate the kind of file
    filename represents.
    """
    ...


def flushCachedNode(nodeName: str, fileDataAlso: bool) -> None:
    ...


def fps() -> float:
    """
    Returns the current playback FPS.
    """
    ...


def frame() -> int:
    """
    Return the current global frame number
    """
    ...


def frameEnd() -> int:
    """
    Returns the maximum frame of the frame range
    """
    ...


def frameStart() -> int:
    """
    Returns the minimum frame of the frame range
    """
    ...


def fullScreenMode(active: bool) -> None:
    """
    Sets the interface to fullscreen or windowed mode.
    """
    ...


def getByteProperty(propertyName: str, start: int, num: int) -> List[int]:
    """
    as a convenience.
    """
    ...


def getCurrentAttributes() -> List[Tuple[str]]:
    """
    Deprecated. Use sourceAttributes() instead.
    """
    ...


def getCurrentImageChannelNames() -> List[str]:
    """
    Deprecated. Use sourceMediaInfo() instead.
    """
    ...


def getCurrentImageSize() -> Tuple[float, float]:
    """
    Deprecated. Use sourceMediaInfo() instead.
    """
    ...


def getCurrentNodesOfType(typeName: str) -> List[str]:
    """
    Deprecated. Use metaEvaluate() instead.
    """
    ...


def getCurrentPixelValue(point: Tuple[float, float]) -> Tuple[float, float]:
    """
    Deprecated. Use sourcePixelValue() instead.
    """
    ...


def getFiltering() -> int:
    """
    Return the filtering method set by setFiltering
    """
    ...


def getFloatProperty(propertyName: str, start: int, num: int) -> List[float]:
    """
    as a convenience.
    """
    ...


def getHalfProperty(propertyName: str, start: int, num: int) -> List[float]:
    """
    as a convenience.
    """
    ...


def getIntProperty(propertyName: str, start: int, num: int) -> List[int]:
    """
    as a convenience.
    """
    ...


def getRendererType() -> str:
    """
    Unused.
    """
    ...


def getSessionType() -> int:
    """
    Deprecated. Use viewNode() instead.
    """
    ...


def getStringProperty(propertyName: str, start: int, num: int) -> List[str]:
    """
    as a convenience.
    """
    ...


def httpGet(url: str, headers: List[Tuple[str, str]], replyEvent: str, authenticationEvent: str, progressEvent: str, ignoreSslErrors: bool, urlIsEncoded: bool) -> None:
    """
    ----
    """
    ...


def httpPost(url: str, headers: List[Tuple[str, str]], postData: List[int], replyEvent: str, authenticationEvent: str, progressEvent: str, ignoreSslErrors: bool, urlIsEncoded: bool) -> None:
    """
    ----
    """
    ...


def httpPut(url: str, headers: List[Tuple[str, str]], putData: List[int], replyEvent: str, authenticationEvent: str, progressEvent: str, ignoreSslErrors: bool, urlIsEncoded: bool) -> None:
    """
    ----
    """
    ...


def imageGeometry(name: str, useStencil: bool) -> List[Tuple[float, float]]:
    """
    Returns an array of image corners for the given source name. These are
    the coordinates of the final rendered image in the view.
    """
    ...


def imageGeometryByIndex(index: int, useStencil: bool) -> List[Tuple[float, float]]:
    """
    Returns an array of image corners. These are the coordinates of the
    final rendered image in the view. The index argument is the same one
    found in the RenderedImageInfo and PixelImageInfo structures.
    """
    ...


def imageGeometryByTag(name: str, value: str, useStencil: bool) -> List[Tuple[float, float]]:
    """
    Returns an array of image corners. These are the coordinates of the
    final rendered image in the view. The tag name and value are used
    to locate the image.
    """
    ...


def imageToEventSpace(sourceName: str, point: Tuple[float, float], normalizedImageOrigin: bool) -> Tuple[float, float]:
    ...


def imagesAtPixel(point: Tuple[float, float], tag: str, sourcesOnly: bool) -> List[PixelImageInfo]:
    """
    Given a point in event space (whatever the Event object returns for
    the pointer value) returns an array of PixelImageInfo objects
    describing the resulting rendered images at that pixel.
    The PixelImageInfo objects will be in order of front to back.
    """
    ...


def inPoint() -> int:
    """
    Returns the in-point. This value will be in the range of frameStart() and frameEnd().
    """
    ...


def inc() -> int:
    """
    Returns the value set by setInc().
    """
    ...


def inputAtPixel(point: Tuple[float, float], strict: bool) -> str:
    ...


def insertByteProperty(propertyName: str, value: List[int], beforeIndex: int) -> None:
    """
    Modify the value of a property in the graph. The value passed in replaces 
    and/or extends parts of the current property value. The optional beforeIndex 
    parameter specifies where the insertion will occur. By default the insertion is 
    at the end of the property value. In other words, the default behavior is to 
    append values onto the end of the property.
    See also: setByteProperty
    """
    ...


def insertCreatePixelBlock(event: Event) -> None:
    """
    Add a block of pixels to an existing RVImageSource node. This is
    only used to pass along data from a "pixel-block" event (usually from
    a network connection to an external process). 
    """
    ...


def insertFloatProperty(propertyName: str, value: List[float], beforeIndex: int) -> None:
    """
    Modify the value of a property in the graph. The value passed in replaces 
    and/or extends parts of the current property value. The optional beforeIndex 
    parameter specifies where the insertion will occur. By default the insertion is 
    at the end of the property value. In other words, the default behavior is to 
    append values onto the end of the property.
    See also: setFloatProperty
    """
    ...


def insertHalfProperty(propertyName: str, value: List[float], beforeIndex: int) -> None:
    """
    Modify the value of a property in the graph. The value passed in replaces 
    and/or extends parts of the current property value. The optional beforeIndex 
    parameter specifies where the insertion will occur. By default the insertion is 
    at the end of the property value. In other words, the default behavior is to 
    append values onto the end of the property.
    See also: setHalfProperty
    """
    ...


def insertIntProperty(propertyName: str, value: List[int], beforeIndex: int) -> None:
    """
    Modify the value of a property in the graph. The value passed in replaces 
    and/or extends parts of the current property value. The optional beforeIndex 
    parameter specifies where the insertion will occur. By default the insertion is 
    at the end of the property value. In other words, the default behavior is to 
    append values onto the end of the property.
    See also: setIntProperty
    """
    ...


def insertStringProperty(propertyName: str, value: List[str], beforeIndex: int) -> None:
    """
    Modify the value of a property in the graph. The value passed in replaces 
    and/or extends parts of the current property value. The optional beforeIndex 
    parameter specifies where the insertion will occur. By default the insertion is 
    at the end of the property value. In other words, the default behavior is to 
    append values onto the end of the property.
    See also: setStringProperty
    """
    ...


def ioFormats() -> List[IOFormat]:
    ...


def ioParameters(extension: str, forEncode: bool, codec: str) -> List[IOParameter]:
    ...


def isBottomViewToolbarVisible() -> bool:
    """
    Returns true if the toolbar is visible
    """
    ...


def isBuffering() -> bool:
    """
    Returns true if the renderer is paused to allow cache buffering to occur.
    """
    ...


def isCaching() -> bool:
    """
    Returns true if images are being cached.
    """
    ...


def isConsoleVisible() -> bool:
    """
    Returns true if the console window is visible.
    """
    ...


def isCurrentFrameError() -> bool:
    """
    Returns true if an error occured trying to render one of the current frames
    """
    ...


def isCurrentFrameIncomplete() -> bool:
    """
    Returns true if one of rendered frames is incomplete (not all pixels are available).
    """
    ...


def isFullScreen() -> bool:
    """
    Returns true if the interface is in fullscreen mode.
    """
    ...


def isMarked(frame: int) -> bool:
    """
    Returns true if the frame is marked in the current view.
    """
    ...


def isMenuBarVisible() -> bool:
    """
    Return true if the menu bar is visible.
    """
    ...


def isModeActive(name: str) -> bool:
    """
    Returns true if the given mode is active.
    """
    ...


def isPlaying() -> bool:
    """
    Returns true if RV is in playback mode.
    """
    ...


def isRealtime() -> bool:
    """
    Returns true if the session is in realtime playback mode
    """
    ...


def isTimerRunning() -> bool:
    """
    Returns true if startTimer() has been called more recently than stopTimer().
    """
    ...


def isTopViewToolbarVisible() -> bool:
    """
    Returns true if the toolbar is visible
    """
    ...


def javascriptMuExport(frame: qt.QWebEnginePage) -> None:
    ...


def licensingState() -> int:
    ...


def loadChangedFrames(sourceNodes: List[str]) -> None:
    """
    For each in the given list of RVFileSource nodes, cached frames that have
    changed on disk since they were last read will be reloaded (including the
    current frame).  Only works with Sources that hold a sequence of image files.
    """
    ...


def loadCount() -> int:
    """
    Number of sources loaded since progressive loading started.
    """
    ...


def loadTotal() -> int:
    """
    Total number of sources to be created during progressive loading.
    """
    ...


def mainViewWidget() -> qt.QWidget:
    """
    Returns a qt.QWidget corresponding to the session's QGLWidget
    """
    ...


def mainWindowWidget() -> qt.QMainWindow:
    """
    Returns the main window Qt widget
    """
    ...


def mapPropertyToGlobalFrames(propName: str, maxDepth: int, root: str) -> List[int]:
    """
    This function performs an inverse mapping of frame numbers stored in a
    property of a node to global frames in the current view. 
    For example if you need to know the global frame corresponding to the local
    frame 100 of media in a source node you can use this function. In this case an
    int property on the source node needs to be created and set to the value 100.
    In the simplist case, calling the function with the appropriate path to the
    property will return an array containing a single value -- the global frame
    corresponding to frame 100.   Note that in the more general case a single
    source frame can correspond to any number of global frames.
    The maxDepth argument can be used to limit the complexity of the
    search -- this is useful if you know that more than one node in the
    evaluation path has the given property. 
    The optional root argument can be used to map to frames relative to
    the given root instead of the true root of the graph. In that case the
    returned frame numbers may not be "global" frames.
    """
    ...


def margins() -> Tuple[float, float, float, float]:
    """
    Return the margin set via setMargins().
    """
    ...


def markFrame(frame: int, mark: bool) -> None:
    """
    Mark or unmark the given frame for the current view node. Marks are
    stored independently on each view node.
    """
    ...


def markedFrames() -> List[int]:
    """
    Returns an array of all frames marked in the current view.
    """
    ...


def mbps() -> float:
    """
    Returns the megabytes/second achieved by supported I/O methods.
    """
    ...


def metaEvaluate(frame: int, root: str, leaf: str, unique: bool) -> List[MetaEvalInfo]:
    """
    This function scans the internal IP graph in the same manner that the
    renderer does, but does not evaluate images in the process. Instead,
    it records the names and evaluation input parameters for each node as
    it is traversed in evaluation order. 
    The frame passed in is usually the current frame, but can be any valid
    global frame number.
    You can optionally specify a root and leaf node between which the
    meta-evaluation occcurs. By default it will use the root of the graph
    and all leaves.
    The function returns an array of MetaEvalInfo structures. 
    """
    ...


def metaEvaluateClosestByType(frame: int, typeName: str, root: str, unique: bool) -> List[MetaEvalInfo]:
    """
    This function is similar to metaEvaluate() in that it scans the graph
    in evaluation order. However, this function will stop scanning
    branches in the IP graph when it encounters a node of type typeName.
    The frame passed in is usually the current frame, but can be any valid
    global frame number.
    The optional root argument causes the function to start scanning at
    that node instead of the true root of the graph. 
    This behavior is similar to how the set*Property() functions decide
    which node to use when given a type name instead of node name. E.g. 
    `setIntProperty("#RVColor.lut.active", int[] {1})` . In that case, all 
    "nearby" RVColor nodes are modified.
    """
    ...


def myNetworkHost() -> str:
    ...


def myNetworkPort() -> int:
    """
    Returns the currently set port number for networking
    """
    ...


def narrowToRange(frameStart: int, frameEnd: int) -> None:
    ...


def narrowedFrameEnd() -> int:
    ...


def narrowedFrameStart() -> int:
    ...


def networkAccessManager() -> qt.QNetworkAccessManager:
    """
    Returns Qt network access manager object
    """
    ...


def newImageSource(mediaName: str, width: int, height: int, uncropWidth: int, uncropHeight: int, pixelAspect: float, channels: int, bitsPerChannel: int, floatingPoint: bool, startFrame: int, endFrame: int, fps: float, layers: List[str], views: List[str]) -> str:
    """
    to add blocks of pixels to it.
    """
    ...


def newImageSourcePixels(sourceName: str, frame: int, layer: str, view: str) -> str:
    """
    Allocate pixels for frame (and possible layer and view) in an existing
    RVImageSource node. This should be called before
    insertCreatePixelBlock().
    """
    ...


def newNode(nodeType: str, nodeName: str) -> str:
    """
    Create a new node of type typeName with name nodeName. If nodeName is 
    the name of an existing node it will be modified to make it unique.
    Returns the name of the new node.
    The event "new-node" will be sent immediately.
    """
    ...


def newProperty(propertyName: str, propertyType: int, propertyWidth: int) -> None:
    """
    Create a new property in the given node with the given name. 
    The type is one of: IntType, FloatType, ShortType, StringType, HalfType, ByteType.
    The width can be greater than 1 if a vector of type is intended. For example, a property 
    that holds 3D points would be a FloatType with width 3.
    See also commands.newNDProperty for a more general property constructor.
    """
    ...


def newSession(files: List[str]) -> None:
    """
    Create a new session from the given files. The files may be media files or a single session file.
    """
    ...


def nextViewNode() -> str:
    """
    The name of the next view node in the view history.
    """
    ...


def nodeConnections(nodeName: str, traverseGroups: bool) -> Tuple[List[str], List[str]]:
    """
    Returns a tuple of two string arrays. The first string array contains
    the names of the nodes connected to the inputs and the second array 
    the names of nodes connected to the outputs.
    """
    ...


def nodeExists(nodeName: str) -> bool:
    """
    Returns true if nodeName exists in the graph
    """
    ...


def nodeGroup(nodeName: str) -> str:
    """
    Return the group node name that nodeName is a memeber of or "" if
    nodeName is a top level node (not in a group). 
    """
    ...


def nodeGroupRoot(nodeName: str) -> str:
    ...


def nodeImageGeometry(nodeName: str, frame: int) -> NodeImageGeometry:
    """
    Return a NodeImageGeometry structure for the given node.
    This structure describes the geometry and timing of the node's output.
    """
    ...


def nodeRangeInfo(nodeName: str) -> NodeRangeInfo:
    ...


def nodeType(nodeName: str) -> str:
    """
    Returns the type name of the given node.
    """
    ...


def nodeTypes(userVisibleOnly: bool) -> List[str]:
    ...


def nodes() -> List[str]:
    """
    Returns an array of strings containing the name of every node in the graph.
    """
    ...


def nodesInGroup(nodeName: str) -> List[str]:
    """
    Returns the names of nodes in the group node nodeName. These nodes
    are by definition not top level nodes.
    """
    ...


def nodesOfType(typeName: str) -> List[str]:
    """
    Returns all nodes of the given type. In addition to exact node type
    names, you can also use #RVSource which will match RVFileSource or
    RVImageSource.
    """
    ...


def ocioUpdateConfig(node: str) -> None:
    ...


def openFileDialog(associated: bool, multiple: bool, directory: bool, filters: List[Tuple[str, str]], defaultPath: str) -> List[str]:
    """
    Launch a generic file dialog to choose files.
    If associated is true on the Mac, the file dialog will be a "sheet" on 
    other platforms it has no effect. 
    The optional multiple parameter indicates whether multiple files or directories may
    be selected.
    The optional directory parameter indicates whether directories or files selection
    should be allowed. If directory is true then files cannot be selected.
    The optional filter parameter is a string of of the form:
    ----
    ext1|desc1|ext2|desc2|ext3|desc3|...
    ----
    The string contains pairs of extensions and descriptions. The pairs
    are separated by pipe characters and the extension and descriptions
    are also separated by pipes.
    Alternate, the second form of the function can take a list of string pairs of `(extension, description)`
    The file browser will add two additional choices for the user: "all relevant files" and "any file".
    The first choice will allow any file that matches an extension in the list. The second will allow
    any file. 
    The optional defaultPath parameter can be used to set the default directory
    shown in the dialog to a location other than the current working directory.
    """
    ...


def openMediaFileDialog(associated: bool, selectType: int, filter: str, defaultPath: str, label: str) -> List[str]:
    """
    Launch the media file dialog to choose files. 
    If associated is true on the Mac, the file dialog will be a "sheet" on 
    other platforms it has no effect. 
    The parameter selectType determines the selection behavior. It can be
    one of: OneExistingFile, ManyExistingFiles, ManyExistingFilesAndDirectories,
    OneFileName, and OneDirectory.
    The optional filter parameter can be a regular expression to restrict the
    files shown.
    The optional defaultPath parameter can be used to set the default directory
    shown in the dialog to a location other than the current working directory.
    The optional label parameter can be set to a descriptive string which is
    displayed in the dialog.
    The function will return after the user has selected one or more files or 
    an exception will be thrown if the user hit the cancel button. 
    An array of the selected file names is returned.
    """
    ...


def openUrl(url: str) -> None:
    """
    Open the given URL using the prefered desktop application (browser, etc)
    """
    ...


def openUrlFromUrl(url: qt.QUrl) -> None:
    ...


def optionsNoPackages() -> int:
    """
    Returns 1 if -nopackages was on the command line.
    """
    ...


def optionsPlay() -> int:
    """
    Returns 1 if -play was on the command line or the preferences are set 
    to play automatically.
    """
    ...


def optionsProgressiveLoading() -> int:
    """
    Deprecated.
    """
    ...


def outPoint() -> int:
    """
    Returns the out-point. This value will be in the range of frameStart() and frameEnd().
    """
    ...


def packageListFromSetting(settingName: str) -> List[str]:
    ...


def play() -> None:
    """
    Put RV into play mode. RV will be in one of realtime or
    play-all-frames (realtime off) mode when it plays. You can set
    realtime mode on or off using setRealtime().
    """
    ...


def playMode() -> int:
    """
    Returns the play mode set by setPlayMode().
    """
    ...


def popEventTable(table: str) -> None:
    """
    Causes the top of the event table stack to be popped off the
    stack. The case in which a table name is specified, the named table is
    removed from the event table stack without further changing the
    ordering of the stack.
    """
    ...


def popupMenu(event: Event, menu: List[MenuItem]) -> None:
    """
    Display a popup menu at the pointer location in event.
    """
    ...


def popupMenuAtPoint(x: int, y: int, menu: List[MenuItem]) -> None:
    ...


def prefTabWidget() -> qt.QTabWidget:
    """
    Returns the preferences tab Qt widget
    """
    ...


def presentationMode() -> bool:
    ...


def previousViewNode() -> str:
    """
    The name of the last view node in the view history.
    """
    ...


def properties(nodeName: str) -> List[str]:
    """
    Return an array of property names in the given node.
    """
    ...


def propertyExists(propertyName: str) -> bool:
    """
    Returns true if the specified property exists
    """
    ...


def propertyInfo(propertyName: str) -> PropertyInfo:
    """
    Returns a PropertyInfo structure describing the given property.
    """
    ...


def pushEventTable(table: str) -> None:
    """
    For all currently active modes, the named event table will be pushed
    onto the event table stack. Since the new table is at the top of the
    stack, it will be considered before any other event table. 
    Event tables of the same name are merged together such that the mode
    ordering determines binding precedence. Table ordering is determined
    by the event table stack. Tables at the top of the stack can shadow
    tables lower on the stack.
    The event table stack is used to implement prefix keys. E.g. when a
    prefix key is pressed it causes a specific event table to be pushed to
    the top of stack thus overriding existing bindings. When the prefix
    actions are complete the table is popped off the stack.
    """
    ...


def putUrlOnClipboard(url: str, title: str, doEncode: bool) -> None:
    """
    Copy a URL on the system clipboard
    """
    ...


def queryDriverAttribute(attribute: str) -> str:
    ...


def readProfile(fileName: str, node: str, usePath: bool, tag: str) -> None:
    """
    Read and apply a profile (a baked description of a GroupNode and all its
    contents). The profile can either be an absolute or relative path to a file or
    a profile name in which case the profile path is searched for a file name like
    "name.profile" or "name.tag.profile" if the tag argument is supplied.
    """
    ...


def readSetting(group: str, name: str, defaultValue: SettingsValue) -> SettingsValue:
    """
    Read a value from the user's settings (preferences) file. The funciton returns a 
    SettingsValueType (a union) which must be unpacked. The group and name parameters
    specify the full path to the settings value. The value parameters supplies a default
    value and type to return if the setting doesn't exist in the file. The type of the 
    setting value in the file must match the type of the default value passed in to
    the function.
    For example:
    ----
    let SettingsValue.Bool b = 
    """
    ...


def redo() -> None:
    ...


def redoHistory() -> List[str]:
    ...


def redoPathSwapVars(pathWithoutVars: str) -> str:
    """
    document me
    """
    ...


def redraw() -> None:
    """
    Calling redraw will queue a redraw request for the session's main view.
    """
    ...


def releaseAllCachedImages() -> None:
    """
    Forces flushing and deallocation of all cached image data.
    """
    ...


def releaseAllUnusedImages() -> None:
    """
    Forces flushing and deallocation of cached image data not currently wired to be used.
    """
    ...


def reload(startFrame: int, endFrame: int) -> None:
    """
    Reload the frame cache. If no arguments are given, the entire frame
    cache will be reloaded. If a global startFrame and endFrame are given,
    only frames between them are reloaded.  For sequences, frames that didn't exist originally, but 
    exist now and are part of the target range, will be loaded.
    """
    ...


def relocateSource(sourceNode: str, oldFileName: str, newFileName: str) -> None:
    """
    Replace media in the current source with new media.
    """
    ...


def remoteApplications() -> List[str]:
    """
    Return an array of the remote application names.
    """
    ...


def remoteConnect(name: str, host: str, port: int) -> None:
    """
    Initiate a remote connection to the contact name on the 
    machine host at the given port. The port is optional.
    """
    ...


def remoteConnections() -> List[str]:
    """
    Return an array of the remote connection names.
    """
    ...


def remoteContacts() -> List[str]:
    """
    Returns an array of the remote contact names. These are typically 
    in the form user@machine, but could also be the names of applications
    like controller@machine.
    """
    ...


def remoteDefaultPermission() -> int:
    """
    The default permission this RV will use for new incoming connections.  One of
    NetworkPermissionAsk, NetworkPermissionAllow, or NetworkPermissionDeny.
    """
    ...


def remoteDisconnect(remoteContact: str) -> None:
    """
    Disconnect from remoteContact.
    """
    ...


def remoteLocalContactName() -> str:
    """
    This RV's contact name as seen by other connected applications.
    """
    ...


def remoteNetwork(on: bool) -> None:
    """
    Turn networking on/off.
    """
    ...


def remoteNetworkStatus() -> int:
    """
    Returns either NetworkStatusOn or NetworkStatusOff.
    """
    ...


def remoteSendDataEvent(event: str, target: str, interp: str, data: List[int], recipients: List[str]) -> None:
    """
    This function is similar to remoteSendEvent, but is used to send blobs
    of binary data instead of plain text. In addition to the parameters it
    shares with remoteSendEvent, this function also takes an interp string
    (which could be e.g. a mime type) and a _raw data parameter instead of
    string contents.
    If the recipient is another RV process, it will send the event to the UI
    as an application event with the data as the Event.dataContents().
    """
    ...


def remoteSendEvent(event: str, target: str, contents: str, recipients: List[str]) -> None:
    """
    Send an event message over the network to recipients. The event message
    is a regular message but with an event name and target name attached. 
    The recipients array should only contain strings obtained from the 
    remoteContacts() function.
    If the recipient is another RV process, it will send the event to the UI
    as an application event with the contents as the Event.contents().
    remoteSendEvent() is more useful than remoteSendMessage() when two RVs
    are communicating.
    """
    ...


def remoteSendMessage(message: str, recipients: List[str]) -> None:
    """
    Send a message over the network to connected applications. The message
    will be sent to each of the recipients. The recipients array should only
    contain strings obtained from the remoteContacts() function.
    remoteSendMessage() is most useful when RV is communicating with a non-RV
    application or controller.
    """
    ...


def renderedImages() -> List[RenderedImageInfo]:
    """
    Returns an array of RenderedImageInfo objects one for each image
    rendered rendered to the view.
    """
    ...


def resetMbps() -> None:
    """
    Reset the running mbps tally.
    """
    ...


def resizeFit() -> None:
    """
    Cause the window geometry to be resized to the contents if possible.
    """
    ...


def rvioSetup() -> None:
    ...


def saveFileDialog(associated: bool, filter: str, defaultPath: str, directory: bool) -> str:
    """
    Launch a generic file or directory save dialog to select 
    a file or directory name for saving.
    If associated is true on the Mac, the file dialog will be a "sheet" on 
    other platforms it has no effect. 
    The optional filter parameter can be a regular expression to restrict the
    files shown.
    The optional defaultPath parameter can be used to set the default directory
    shown in the dialog to a location other than the current working directory.
    The optional directory parameter indicates whether directories or files selection
    should be allowed. If directory is true then files cannot be selected.
    """
    ...


def saveSession(fileName: str, compressed: bool, sparse: bool) -> None:
    """
    Save a .rv file for the session as fileName. If asACopy is true then
    fileName will not become the session's file name. If compressed is
    true the file will be a compressed binary .rv file otherwise it will
    be human readable UTF-8 encoded text file.  If sparse is true the
    session file will not contain most properties that still have their
    default values.
    """
    ...


def scrubAudio(on: bool, chunkDuration: float, loopCount: int) -> None:
    """
    Turn on/off audio scrubbing. The optional chunkDuration parameter
    indicates the smallest length of contiguous audio that will be
    played. The optional loopCount parameter indicates how many times a
    chunk will be played if the user is holding the mouse down but not
    moving it. 
    """
    ...


def sendInternalEvent(eventName: str, contents: str, senderName: str) -> str:
    """
    Send an application generated event to the user interface. The event
    is sent immediately. The event can optionally be sent with a string
    that is retrievable via the Event.contents() function and a senderName
    retrievable with the Event.sender() function.
    """
    ...


def sequenceOfFile(file: str) -> Tuple[str, int]:
    """
    Given a single file name, the function will attempt to find a sequence
    on the file system than contains it.
    """
    ...


def sessionFileName() -> str:
    """
    Returns the name of the .rv file associated with the session.
    """
    ...


def sessionFromUrl(url: str) -> None:
    """
    Create a session from a (possibly baked) rvlink URL
    """
    ...


def sessionName() -> str:
    """
    Returns the name as seen by the user of the current session.
    """
    ...


def sessionNames() -> List[str]:
    """
    Returns the names of all sessions being managed by the current RV process.
    """
    ...


def setAudioCacheMode(mode: int) -> None:
    """
    Set the audio cache mode. Accepted values are CacheOff, CacheGreedy, or CacheBuffer.
    """
    ...


def setByteProperty(propertyName: str, value: List[int], allowResize: bool) -> None:
    """
    as a convenience.
    """
    ...


def setCacheMode(mode: int) -> None:
    """
    Sets the frame caching mode. Accepted values are CacheOff, CacheGreedy, or CacheBuffer.
    """
    ...


def setCacheOutsideRegion(cacheOutside: bool) -> None:
    ...


def setCursor(cursorType: int) -> None:
    """
    Set the view cursor to one of: CursorNone, CursorArrow, or CursorDefault.
    Alternately, setCursor can accept one of the Qt cursor symbolic constants.
    """
    ...


def setDriverAttribute(attribute: str, value: str) -> None:
    ...


def setFiltering(filterType: int) -> None:
    """
    Set the OpenGL filtering type. This is currently either GL_NEAREST or GL_LINEAR.
    """
    ...


def setFloatProperty(propertyName: str, value: List[float], allowResize: bool) -> None:
    """
    as a convenience.
    """
    ...


def setFrame(frame: int) -> None:
    """
    Set the current frame. This will trigger a frame-changed event.
    """
    ...


def setFrameEnd(frame: int) -> None:
    """
    Set the frame range end frame.
    """
    ...


def setFrameStart(frame: int) -> None:
    """
    Set the frame range start frame.
    """
    ...


def setHalfProperty(propertyName: str, value: List[float], allowResize: bool) -> None:
    """
    as a convenience.
    """
    ...


def setHardwareStereoMode(active: bool) -> None:
    """
    Turn on/off quad-buffered OpenGL stereo.
    """
    ...


def setInPoint(frame: int) -> None:
    """
    Set the out-point. The value is clamped to the range [frameStart(), frameEnd()].
    """
    ...


def setInc(inc: int) -> None:
    """
    Set the playback increment. This is typically -1 or 1 indicating forward or reverse playback.
    """
    ...


def setIntProperty(propertyName: str, value: List[int], allowResize: bool) -> None:
    """
    as a convenience.
    """
    ...


def setMargins(margins: Tuple[float, float, float, float], allDevices: bool) -> None:
    """
    Set the rendering margins. This is a single float[4] vector encoding
    the left, right, top and bottom margin sizes.  If any provided margin value is -1 the existing
    margin value will not be changed.  The margins on only the "event device" will be changed
    unless the allDevices arg is true.
    """
    ...


def setNodeInputs(nodeName: str, inputNodes: List[str]) -> None:
    """
    Set the inputs for nodeName to the inputNodes. 
    If the inputs would cause a cycle in the graph an exception will be thrown.
    The event "graph-node-inputs-changed" will be sent immediately.
    """
    ...


def setOutPoint(frame: int) -> None:
    ...


def setPlayMode(mode: int) -> None:
    """
    Sets the looping behavior during playback. The accepted values are
    PlayLoop, PlayOnce, and PlayPingPong.
    """
    ...


def setPresentationMode(value: bool) -> None:
    ...


def setRealtime(realtime: bool) -> None:
    """
    Turn on/off realtime playback mode
    """
    ...


def setRemoteDefaultPermission(permission: int) -> None:
    """
    Set the default permission this RV will use for new incoming connections.  One of
    NetworkPermissionAsk, NetworkPermissionAllow, or NetworkPermissionDeny.
    If the network is active, this change will not take effect until the next
    activation.
    """
    ...


def setRemoteLocalContactName(name: str) -> str:
    """
    Set this RV's contact name as seen by other connected applications.
    If the network is active, this change will not take effect until the next
    activation.
    """
    ...


def setRendererType(name: str) -> None:
    """
    Unused.
    """
    ...


def setSessionName(name: str) -> None:
    """
    Set the user visible session name.
    """
    ...


def setSessionType(sessionType: int) -> None:
    """
    Deprecated. Use setViewNode() instead.
    """
    ...


def setSourceMedia(sourceNode: str, fileNames: List[str], tag: str) -> None:
    """
    Replace all media in the given RVFileSource node with new media with optional tag.
    """
    ...


def setStringProperty(propertyName: str, value: List[str], allowResize: bool) -> None:
    """
    as a convenience.
    """
    ...


def setViewNode(nodeName: str) -> None:
    """
    Set nodeName to be the current view node. The node must be a top level node.
    """
    ...


def setViewSize(width: int, height: int) -> None:
    """
    Set the view area size to an exact width and height.
    """
    ...


def setWindowTitle(title: str) -> None:
    """
    Set the title of the window to title. On linux the window manager can override this.
    """
    ...


def shortAppName() -> str:
    ...


def showBottomViewToolbar(show: bool) -> None:
    """
    Show/Hide bottom view toolbar
    """
    ...


def showConsole() -> None:
    """
    Show the console window
    """
    ...


def showNetworkDialog() -> None:
    ...


def showTopViewToolbar(show: bool) -> None:
    """
    Show/Hide top view toolbar
    """
    ...


def skipped() -> int:
    """
    Returns the number of frames skipped during playback. This will be 0
    if no frames were skipped.
    """
    ...


def sourceAtPixel(p: Tuple[float, float]) -> List[PixelImageInfo]:
    ...


def sourceAttributes(sourceName: str, mediaName: str) -> List[Tuple[str, str]]:
    """
    Returns an array of image attribute name/value pairs at the current
    frame. The sourceName can be the node name or the source path as
    returned by PixelImageInfo, etc. The optional media argument can be
    used to constrint the attributes to that media only.
    Returns an empty array if no attributes are found.
    """
    ...


def sourceDataAttributes(sourceName: str, mediaName: str) -> List[Tuple[str, bytearray]]:
    """
    Returns an array of image attribute name/value pairs at the current
    frame for data blob attributes. The sourceName can be the node name or the source path as
    returned by PixelImageInfo, etc. The optional media argument can be
    used to constrint the attributes to that media only.
    The name/value pair value is a byte[]. Unlike the sourceAttributes() function, 
    only attributes of that type are returned. One use case is
    to obtain the data blob for an embedded ICC profile.
    Returns an empty array if no attributes are found.
    """
    ...


def sourceDisplayChannelNames(sourceName: str) -> List[str]:
    """
    Returns the names of channels in a source which are mapped to the display RGBA channels.
    """
    ...


def sourceGeometry(name: str) -> List[Tuple[float, float]]:
    ...


def sourceMedia(sourceName: str) -> Tuple[str, List[str], List[str]]:
    """
    Return the media in the given source and a list of views and layers if
    the media has any. For example a single EXR file in a source may have
    multiple layers (e.g. diffuse, specular, etc) as well as multiple
    views (e.g. left, right eyes). 
    """
    ...


def sourceMediaInfo(sourceName: str, mediaName: str) -> SourceMediaInfo:
    """
    Returns a SourceMediaInfo structure for the given source and optional
    media. The SourceMediaInfo supplies geometric and timing information
    about the image and sequence.
    """
    ...


def sourceMediaInfoList(nodeName: str) -> List[SourceMediaInfo]:
    ...


def sourceNameWithoutFrame(name: str) -> str:
    ...


def sourcePixelValue(sourceName: str, px: float, py: float) -> Tuple[float, float, float, float]:
    """
    Returns the value of a pixel in the original source image. This value
    is converted to Rec. 709 RGBA form. The coordinates passed in are
    relative to the original image pixel space.
    """
    ...


def sources() -> List[Tuple[str, int, int, int, float, bool, bool]]:
    """
    Returns an array of source media. The return tuples contain:
    |==================================
    | string | file name
    | int    | start frame
    | int    | end frame
    | int    | frame increment
    | float  | fps
    | bool   | true if media has audio
    | bool   | true if media has video
    |==================================
    """
    ...


def sourcesAtFrame(frame: int) -> List[str]:
    """
    Returns an array of the names of source nodes (RVFileSource or
    RVImageSource) which would be evaluated at the given frame.
    """
    ...


def sourcesRendered() -> List[RenderedImageInfo]:
    ...


def spoofConnectionStream(streamFile: str, timeScale: float, verbose: bool) -> None:
    """
    When given the path of a file containing a stored RV connection stream, and a
    "timeScale", the stored stream will be "replayed" by RV as if it was an
    external connection.  The timeScale parameter can be used to scale the timing
    of the spooffed connection stream; for example, a timeScale of 0.5 will play
    back the connection in approximately half the time.  A timeScale of 1.0 will
    play back the connection in roughly the time it took to record it.  A timeScale
    of 0.0 will cause the stream to be replayed "as fast as possible".
    """
    ...


def startTimer() -> None:
    """
    Start user timer.
    """
    ...


def startupResize() -> bool:
    ...


def stereoSupported() -> bool:
    """
    Returns true if quad-buffered stereo is supported by the hardware
    """
    ...


def stop() -> None:
    """
    Stop playback mode.
    """
    ...


def stopTimer() -> float:
    """
    Stop the user timer.
    """
    ...


def testNodeInputs(nodeName: str, inputNodes: List[str]) -> str:
    """
    Return true if setting the inputs of nodeName to inputsNodes would cause
    an exception to be thrown.
    """
    ...


def theTime() -> float:
    """
    The time in seconds since the application started.
    """
    ...


def toggleMenuBar() -> None:
    """
    Toggle menu bar visibility. Has no effect on the Mac.
    """
    ...


def unbind(modeName: str, tableName: str, eventName: str) -> None:
    """
    Removes the event binding from the given mode's event table.
    """
    ...


def unbindRegex(modeName: str, tableName: str, eventName: str) -> None:
    """
    Removes the regular expression event binding from the given mode's
    event table.
    """
    ...


def undo() -> None:
    ...


def undoHistory() -> List[str]:
    ...


def undoPathSwapVars(pathWithVars: str) -> str:
    """
    document me
    """
    ...


def validateShotgunToken(port: int, tag: str) -> str:
    ...


def videoState() -> VideoDeviceState:
    ...


def viewNode() -> str:
    """
    Returns the currently viewed node.
    """
    ...


def viewNodes() -> List[str]:
    """
    Returns an array of the nodes which can be viewed.
    """
    ...


def viewSize() -> Tuple[float, float]:
    """
    The size in pixels of the viewing window
    """
    ...


def watchFile(filename: str, watch: bool) -> None:
    """
    will give the name of the changed file.
    """
    ...


def writeAllNodeDefinitions(fileName: str, inlineSourceCode: bool) -> None:
    ...


def writeNodeDefinition(nodeName: str, fileName: str, inlineSourceCode: bool) -> None:
    ...


def writeProfile(fileName: str, node: str, comments: str) -> None:
    """
    Given a node write a Profile (a baked description of a GroupNode and all its
    contents) for that node to a file.
    """
    ...


def writeSetting(group: str, name: str, value: SettingsValue) -> None:
    """
    This module provides the most atomic API to RV. Other modules like `extra_commands` , `rvtypes` , 
    `rvui` , etc are built using the `commands` module API.
    
    """
    ...


