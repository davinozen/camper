class ChannelInfo:
    ...


class ChapterInfo:
    ...


class Event:
    ...


class IOFormat:
    ...


class IOParameter:
    ...


class LayerInfo:
    ...


class MenuItem:
    ...


class MetaEvalInfo:
    ...


class NodeImageGeometry:
    ...


class NodeRangeInfo:
    ...


class PixelImageInfo:
    ...


class PropertyInfo:
    ...


class RenderedImageInfo:
    ...


class SettingsValue:
    ...


class SourceMediaInfo:
    ...


class VideoDeviceState:
    ...


class ViewInfo:
    ...
